#!/bin/bash

for i in {1..2}
do
  for (( j=128; j<=1024; j+=128))
  do
	echo "i $i j $j"
	./sten3d $j $i >> sten_out_128_1024.csv
  done
done
