#!/usr/bin/env python

import math
import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objs as go
import plotly.io as pio
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Do this to save directly to file
#plotly.io.orca.config.executable = "/home/thiago/orca-1.2.1-x86_64.AppImage"
#plotly.io.orca.config.save()

filename="baseline_fftw_thunder.res"
outfigbase="fftw_thunder_"
data = pd.read_csv(filename, sep=" ", header=None)

data.columns = ["N","N","len","plstrg", "N", "pltime", "N", "N", "etmin", "N",
        "etmax", "N", "etavg", "N", "N", "N", "niter"]

#print (data["plstrg"])


def plotmyfig(data, datalbl, dxfield='len', dyfield='etmin', title="Not def",
        xlabel="xlnone",
        ylabel="ylnone", xscale="linear", xscalebase=10.0, yscale="linear",
        yscalebase=10.0, xticksval=None, xtickslbl=None,
        legloc="upper left", legncol=2,
        ylim=None,
        figname=None):

    plt.figure()
    for i, d in enumerate(data):
        plt.scatter(x=d[dxfield], y=d[dyfield], color=datalbl[i][1],
                label=datalbl[i][0])
    #endfor
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)

    if xscale == 'log':
        plt.xscale(xscale, basex=xscalebase)
    else:
        plt.xscale(xscale)

    if yscale == 'log':
        plt.yscale(yscale, basey=yscalebase)
    else:
        plt.yscale(yscale)

    if xticksval is not None:
        if xtickslbl is not None:
            print("xticks should be there", xticksval, xtickslbl)
            plt.xticks(xticksval, xtickslbl)
        else:
            plt.xticks(xticksval)
    #endif

    if ylim is not None:
        plt.ylim(ylim)

    plt.legend(loc=legloc, ncol=legncol)
    if figname is not None:
        plt.savefig(figname)
        print("Saving fig ",figname,".")
    #endif
    print ("Done")
#enddef
ref="FFTW_ESTIMATE"
lstref=[]

gp= data.groupby('len')
#print(gp)
refpllst = []
refexavglst = []
for name, v in gp:
    print("*******")
    print("name:", name, "\nv:",v['pltime'], type(v['pltime']),'\n')
    refv = v.query('plstrg == "'+ref+'"')['pltime']
    refv2 = v.query('plstrg == "'+ref+'"')['etavg']
    mys = pd.Series([refv.iloc[0]]*len(v['pltime']))
    mys2 = pd.Series([refv2.iloc[0]]*len(v['etavg']))
    d = v['pltime'] / mys.values
    d2 = v['etavg'] / mys2.values
    print("d:",d)
    refpllst.append(d)
    refexavglst.append(d2)

#print(data)
data['refpl'] = pd.concat(refpllst)
data['refetavg'] = pd.concat(refexavglst)
print(data)

est = data.query('plstrg == "FFTW_ESTIMATE" and len <= 1048576')
mea = data.query('plstrg == "FFTW_MEASURE" and len <= 1048576')
pat = data.query('plstrg == "FFTW_PATIENCE" and len <= 1048576')
exh = data.query('plstrg == "FFTW_EXHAUST" and len <= 1048576')
data = [est, mea, pat, exh]
datalbl = [("FFTW_ESTIMATE", "r"), ("FFTW_MEASURE","b"), ("FFTW_PATIENCE", "g"), ("FFTW_EXHAUST", "m")]

plotmyfig(data[::-1],datalbl[::-1], dyfield='pltime', title="FFTW Planning Time",
#plotmyfig(data,datalbl, dyfield='pltime', title="FFTW Planning Time",
         ylabel="Elapsed Time (milisec)",
         yscale="log",
         xscale="log", xscalebase=2.0,
         #legloc="lower right",
         legloc="upper left",
         legncol=1,
         ylim=[0.6,2800000],
         figname=outfigbase+'plantime_loglog.png')

plotmyfig(data, datalbl, title="FFTW Min Execution Time",
          ylabel="Elapsed Time (milisec)",
          xlabel="Input length",
          xscale="log", xscalebase=2.0,
          figname=outfigbase+"execmintime_loglin.png")

plotmyfig(data, datalbl, title="FFTW Min Execution Time",
          ylabel="Elapsed Time (milisec)",
          xlabel="Input length",
          xticksval=[64]+[2**x for x in range(16,21)],
          xtickslbl=['$2^{6}$']+["$2^{"+str(x)+"}$" for x in range(16,21)],
          figname=outfigbase+"execmintime_linlin.png")

plotmyfig(data, datalbl, title="FFTW Avg Execution Time",
          dyfield="etavg",
          ylabel="Elapsed Time (milisec)",
          xlabel="Input length",
          xticksval=[64]+[2**x for x in range(16,21)],
          xtickslbl=['$2^{6}$']+["$2^{"+str(x)+"}$" for x in range(16,21)],
          figname=outfigbase+"execavgtime_linlin.png")

### Bars
plt.figure()
width=.40
barlabels=["$2^{"+str(int(math.log2(v)))+"}$" for i,v in exh.len.items()]
print (barlabels)
step=2
x = np.arange(step,step*(len(barlabels)+1),step)
print("UOW",x)
#x = np.arange(len(barlabels))
#print("UOW2",x)

#print("UOW",exh['refpl'],pat.refpl)
useref = False
dyfield='pltime' #'refpl'

plt.bar(x-width*(3/2), est[dyfield], width, label="FFTW_ESTIMATE")
plt.bar(x-width/2, mea[dyfield], width, label="FFTW_MEASURE")
plt.bar(x+width/2, pat[dyfield], width, label="FFTW_PATIENCE")
plt.bar(x+width*(3/2), exh[dyfield], width, label="FFTW_EXHAUST")

plt.yscale("log")
plt.xticks(x, barlabels)
plt.legend(loc="upper left")
plt.ylabel("Search Time (milisec)")
plt.xlabel("Number of elements (1D complex double-precision FFT)")
plt.title("FFTW Planning Time")
plt.savefig(outfigbase+'plantime_bar.png')

plt.figure()
width=.40
barlabels=["$2^{"+str(int(math.log2(v)))+"}$" for i,v in exh.len.items()]
print (barlabels)
step=2
x = np.arange(step,step*(len(barlabels)+1),step)
print("UOW",x)
#x = np.arange(len(barlabels))
#print("UOW2",x)

#print("UOW",exh['refpl'],pat.refpl)
useref = False
dyfield='refetavg' #'etavg'#'pltime' #'refpl'

plt.bar(x-width*(3/2), est[dyfield], width, label="FFTW_ESTIMATE")
plt.bar(x-width/2, mea[dyfield], width, label="FFTW_MEASURE")
plt.bar(x+width/2, pat[dyfield], width, label="FFTW_PATIENCE")
plt.bar(x+width*(3/2), exh[dyfield], width, label="FFTW_EXHAUST")

#plt.yscale("log")
plt.xticks(x, barlabels)
plt.ylim([0.0,1.4])
plt.legend(loc="upper left", ncol=2)
plt.ylabel("Execution Time relative to "+ref)
plt.xlabel("Number of elements (1D complex double-precision FFT)")
plt.title("FFTW Execution Time")
plt.savefig(outfigbase+'execavgtime_bar.png')

# Use matplotlib
#plt.figure();
#est = data.query('plstrg == "FFTW_ESTIMATE" and len <= 1048576')
#mea = data.query('plstrg == "FFTW_MEASURE" and len <= 1048576')
#pat = data.query('plstrg == "FFTW_PATIENCE" and len <= 1048576')
#exh = data.query('plstrg == "FFTW_EXHAUST" and len <= 1048576')
#plt.scatter(x=exh.len, y=exh.pltime, color='m', label='FFTW_EXHAUST')
#plt.scatter(x=pat.len, y=pat.pltime, color='g', label='FFTW_PATIENCE')
#plt.scatter(x=mea.len, y=mea.pltime, color='b', label='FFTW_MEASURE')
#plt.scatter(x=est.len, y=est.pltime, color='r', label='FFTW_ESTIMATE')
#plt.title("FFTW Planning Time")
#plt.ylabel("Elapsed Time (milisec)")
#plt.xlabel("Input length (powers of two)")
#plt.yscale("log")
##plt.xscale("log")
##plt.ylim([0.1,2800000])
#plt.legend(loc='lower right', ncol=2)
##data.plot(x='len', y='pltime', kind='line' )
##plt.show();
#plt.savefig('fftw_plantime_linlog.png')

#plt.figure()
#plt.scatter(x=exh.len, y=exh.etmin, color='m', label='FFTW_EXHAUST')
#plt.scatter(x=pat.len, y=pat.etmin, color='g', label='FFTW_PATIENCE')
#plt.scatter(x=mea.len, y=mea.etmin, color='b', label='FFTW_MEASURE')
#plt.scatter(x=est.len, y=est.etmin, color='r', label='FFTW_ESTIMATE')
#plt.title("FFTW Execution Time")
#plt.ylabel("Elapsed Time (milisec)")
#plt.xlabel("Input length")
##maxxval=est.len.max()
#p2lbls =[math.log(x,2) for x in est.len]
##print (maxxval, p2lbls)
##plt.xticks(est.len,p2lbls)
##plt.yscale("log")
#plt.xscale("log",basex=2.0)
##plt.ylim([0.1,2800000])
#plt.legend(loc='upper left', ncol=1)
#plt.savefig('fftw_execmintime_loglin.png')
#
##plt.xticks(p2lbls)
#plt.xscale("linear")
#ntickval = [2**x for x in range(16,21)]
#nticklbl = ["$2^{"+str(x)+"}$" for x in range(16,21)]
#plt.xticks([64]+ntickval, ["$2^{6}$"]+nticklbl)
#plt.savefig('fftw_execmintime_linlin.png')

#plt.figure()
#fig, ax = plt.subplots()
#ax.scatter(x=exh.len, y=exh.etmin, color='m', label='FFTW_EXHAUST')
#ax.scatter(x=pat.len, y=pat.etmin, color='g', label='FFTW_PATIENCE')
#ax.scatter(x=mea.len, y=mea.etmin, color='b', label='FFTW_MEASURE')
#ax.scatter(x=est.len, y=est.etmin, color='r', label='FFTW_ESTIMATE')
#ax.set_title("XXX FFTW Execution Time")
#ax.set_ylabel("Elapsed Time (milisec)")
##ax.set_xlabel("Input length [$2^{x}$]")
#ax.set_xlabel("Input length")
#ax.set_xscale("log", basex=2.0)
##ax.set_xticks(est.len,p2lbls)
##logfmt = matplotlib.ticker.LogFormatterExponent(base=2.0, labelOnlyBase=True)
##ax.xaxis.set_major_formatter(logfmt)
#plt.savefig('fftw_XXXexecmintime.png')

# plotly.express
#fig = px.line(data, x="len", y="pltime", color='plstrg', title="FFTW Planning time",
#fig = px.scatter(data, x="len", y="pltime", color='plstrg', title="FFTW Planning time",
#              labels={'pltime': 'Elapsed Time (ms)',
#                      'plstrg': 'Search Strategy',
#                      'len': 'Input length'})
#fig.update_layout(legend_orientation="h")

#dtexh = data.query('plstrg == "FFTW_EXHAUST"')
#print (dtexh)
#chardata = go.Scatter(x=dtexh.len, y=dtexh.pltime)
#pio.show(chardata)

# write to file
#pio.write_image(fig, file='plotly_static_image.png', format='png')

# open a web browser
#fig.show()

