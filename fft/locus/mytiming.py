import re

def getTiming(std, error):
    r = re.search('.*FFTW n: .*',std)
    m = []
    if r:
       m = re.split('\s+', r.group())
    result = 'N/A'
    if m and len(m) >= 8:
       result = float(m[8])

    return result


#inp = r"""fftw n 128 plan flag ESTIMATE
#FFTW_ESTIMATE...
#[mkplan_ctdit_prol] r= 64 m= 2 n= 128
#(dft-ct-dit/64
#  (dftw-direct-64/252 "t1fv_64_avx")
#  (dft-direct-2-x64 "n2fv_2_avx"))
#FFTW_LOCUS plan 0.35501 (ms)
#Res: p 128.000000 CORRECT
#FFTW n: 128 FFTW_LOCUS plan: 0.35501 exec: min= 0.00095 max= 0.00310 avg= 0.00143 (ms) | iteration 5
#"""
#print getTiming(inp, "");
