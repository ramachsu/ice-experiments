#!/usr/bin/env bash


. ./runparams.sh

search_func() {
    local idx=${1:?Must provide index array }
    xparam="--exprec --saveexprec --stop-after ${stopaf[$idx]}" #--ntests ${ntests} # "--upper-limit ${uplimit[$idx]} --exprec --saveexprec --ntests 10" #

    #ice-locus-hyperopt.py \
    $search \
            -t $locusfile -f ${cfile} --tfunc mytiming.py:getTiming \
            -o suffix -u '.ice' --search $xparam 

    #--convonly --exprec \
}

#for exp in $lenExpon; do
for idx in ${!lenExpon[*]}; do
    #len=$((2**${exp}))
    len=$((2**${lenExpon[$idx]}))
    echo Searching fft len= ${len} uplimit= ${uplimit[$idx]} stopaf= ${stopaf[$idx]} stool= $search run= $run
    export ICELOCUS_FFT_LEN=$len #1048576 #524288  #1024  #262144 #131072 #32768 #256 #
    #locusfile=test2.locus  #fftw-vs4.locus

    search_func $idx
done
