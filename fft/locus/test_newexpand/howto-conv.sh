#!/usr/bin/env bash

locusfile=test2.locus  #fftw-vs4.locus
ice-locus-hyperopt.py --convonly --exprec \
	--saveexprec \
	-t $locusfile -f fftwbase.cpp --tfunc mytiming.py:getTiming \
	-o suffix -u '.ice' --search
