#!/usr/bin/env bash

. ./runparams.sh

ice-locus-hyperopt.py -t $locusfile -f $cfile -o suffix -u ".ice" --convonly --search --exprec --debug
