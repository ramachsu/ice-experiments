#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <kernel/ifftw.h>
#include <api/api.h>
#include <dft/ct.h>

#ifdef __cplusplus
extern "C"  /* extern "C" */
{
#endif /* __cplusplus */
extern double mysecond();
#ifdef __cplusplus
}  /* extern "C" */
#endif /* __cplusplus */

#define BUFNAMESIZE 32
#define TSCALE (1.0e3)
#define TLEG "ms"
#define REAL 0
#define IMAG 1
#define MAXNAM 64
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
#define N0(nembed)((nembed) ? (nembed) : &n)


void print_array(int n, fftw_complex *out)
{
    char bufname[32];
    snprintf(bufname, BUFNAMESIZE, "answer-fftw-%d.txt", n);
    FILE *fout = fopen(bufname,"w");
    if(!fout){
        fprintf(stderr,"ERROR! Could not open %s to output results!\n", bufname);
        exit(1);
    }
    for(int i=0; i<n; i++) {
        fprintf(fout, "%lf %lf ", out[i][REAL], out[i][IMAG]);
    }
    fprintf(fout, "\n");
    fclose(fout);
}

int main(int argc, char **argv) {

    double min=DBL_MAX, max=DBL_MIN, avg=0.0, t_all = 0.0, t_it;
    double t_start, t_end;
    //fftw_plan theplan = 0;

    int n = 32, flagidx = 0, iterations = 5;
    if(argc > 1) {
        n = atoi(argv[1]);
    }
    if(argc > 2) {
        flagidx = atoi(argv[2]);
    }
    if(flagidx > 3) {fprintf(stderr,"Only 4 flags available (2nd argument from 0 to 3).\n");}

    unsigned int flagarr[] = {FFTW_ESTIMATE, FFTW_MEASURE, FFTW_PATIENT, FFTW_EXHAUSTIVE};
    int sign = FFTW_FORWARD;
    unsigned flags = flagarr[flagidx];
    const char *pldesc[] = {"ESTIMATE", "MEASURE", "PATIENCE", "EXHAUST"};
    printf("fftw n %d plan flag %s\n", n, pldesc[flagidx]);

    fftw_complex *in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);
    fftw_complex *out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * n);

    printf("FFTW_%s...\n", pldesc[flagidx]);

    t_start = mysecond();
#pragma @ICE block=myfft
pldesc[flagidx] = "LOCUS";
int howmany = 1, rank = 1, istride = 1, idist = 1, ostride = 1, odist = 1;
const int *inembed = 0;
const int *onembed = 0;

R *ri, *ii, *ro, *io;
if (!fftw_many_kosherp(rank, &n, howmany)){printf("Eita something weird!\n");exit(1);}

EXTRACT_REIM(FFTW_FORWARD, in, &ri, &ii);
EXTRACT_REIM(FFTW_FORWARD, out, &ro, &io);

tensor *sz = fftw_mktensor_rowmajor(rank, &n, N0(inembed), N0(onembed), 2 * istride, 2 * ostride);
tensor *vecsz = fftw_mktensor_1d(howmany, 2 * idist, 2 * odist);
problem * pbl_t = fftw_mkproblem_dft_d(sz, vecsz,
                                    TAINT_UNALIGNED(ri, flags),
                                    TAINT_UNALIGNED(ii, flags),
                                    TAINT_UNALIGNED(ro, flags),
                                    TAINT_UNALIGNED(io, flags));

planner *ego = fftw_mkapiplan_locus_prol(flags, pbl_t);
if(!ego){fprintf(stderr, "Error creating planner!\n");exit(1);};
solver *sW_t = fftw_findSolver(ego, "fftw_codelet_t2fv_4_avx", 0);
if(!sW_t){fprintf(stderr, "Solver sW_t not found!\n"); exit(1);}
ctditinfo *inf_t1 = fftw_mkplan_ctdit_prol(sW_t, pbl_t, ego);
const problem *pbl_t1 = (const problem *) inf_t1->cld_prb;
if(!inf_t1) {fprintf(stderr, "CT inf is not applicable.\n" ); exit(1);}
plan *cldw_t = ((const ct_solver *) sW_t)->mkcldw((const ct_solver*)sW_t,
           inf_t1->r, inf_t1->m * inf_t1->d[0].os, inf_t1->m * inf_t1->d[0].os,
           inf_t1->m, inf_t1->d[0].os,
           inf_t1->v, inf_t1->ovs, inf_t1->ovs,
           0, inf_t1->m,
           inf_t1->p->ro, inf_t1->p->io, ego);
if(!cldw_t) {fprintf(stderr, "No cldw_t plan!\n"); exit(1);}
solver *s_t1 = fftw_findSolver(ego, "fftw_codelet_n1fv_64_avx", 0);
if(!s_t1){fprintf(stderr, "Solver s_t1 not found!\n"); exit(1);}
plan *cld_t1 = s_t1->adt->mkplan(s_t1, pbl_t1, ego);
if(!cld_t1){fprintf(stderr, "CLD cld_t1 mkplan failed!\n"); exit(1);}
plan *cld_t = fftw_mkplan_ctdit_epil(sW_t, cldw_t, cld_t1, inf_t1);
if(!cld_t) {fprintf(stderr, "CT _t plan is null!\n"); exit(1);}
fftw_destroy_ctdit_info(inf_t1);
fftw_plan theplan = fftw_mkapiplan_locus_epil(sign, ego, cld_t, pbl_t);
#pragma @ICE endblock
    t_end = mysecond();

    if(theplan) fftw_print_plan(theplan);

    double t_plan = (t_end-t_start)*TSCALE;
    printf("\nFFTW_%s plan %7.5lf (%s)\n", pldesc[flagidx], t_plan, TLEG);

    for(int i = 0; i < n; ++i) {
        in[i][REAL] = 1.0;
        in[i][IMAG] = 0.0;
    }

    if(theplan) {
        for (int it = 0; it < iterations; it++) {
            t_start = mysecond();
            fftw_execute(theplan); /* repeat as needed */
            t_end = mysecond();
            //double t_exec = (t_end-t_start)*TSCALE;
            //printf("FFTW_%s exec %7.5lf (%s)\n", plandesc, t_exec, TLEG);

            t_it = t_end - t_start;
            if (t_it > 0 && t_it < min) min = t_it;
            if (t_it > 0 && t_it > max) max = t_it;
            t_all += t_it;

            if (it==0) {
                printf("Res: p %f %s\n",out[0][REAL], out[0][REAL] == n ? "CORRECT" : "WRONG");
                if(fopen(".test", "r")) {
                    print_array(n, out);
                }
            }
        }
        avg = t_all/iterations;
    }

    printf("FFTW n: %d FFTW_%s plan: %7.5lf "
           "exec: min= %7.5lf max= %7.5lf avg= %7.5lf (%s) | iteration %d\n",
      n, pldesc[flagidx], t_plan, min*TSCALE, max*TSCALE, avg*TSCALE, TLEG, iterations);

    fftw_destroy_plan(theplan);
    fftw_free(in); fftw_free(out);
}
