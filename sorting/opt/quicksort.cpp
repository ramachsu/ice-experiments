#include "quicksort.h"

/**
 * Quicksort.
 * @param a - The array to be sorted.
 * @param first - The start of the sequence to be sorted.
 * @param last - The end of the sequence to be sorted.
*/
void quicksort(int first, int last, int *a) 
{
    int pivotElement;

    if(first < last)
    {
        //pivotElement = pivot(first, last, a);
        //pivotElement = partition(a, first, last); 
        pivotElement = partitionNew(a, first, last); 
        quicksort(first, pivotElement, a);
        quicksort(pivotElement+1, last, a);
    }
}

int partitionPiv(int a[], int l, int u, int pivot)
{
    int i, j;
    int v, temp;
    //std::cout << "New l: " << l << " u: " << u  << " pivot: " << pivot << std::endl << std::flush;
    v = a[pivot];
    i = l - 1;
    j = u + 1;

    while(true) {
      do 
	i++;
      while(a[i] < v && i <= u);

      do
	j--;
      while(a[j] > v);

      if (i >= j) {
	return j;
      }

      //std::cout << "New i: " << i << " j: " << j << std::endl << std::flush;
      temp=a[i];
      a[i]=a[j];
      a[j]=temp;
    }

}

int partitionNew(int a[], int l, int u)
{
    int i, j;
    int v, temp;
    int pivot = l + (u-l) / 2;
    //std::cout << "New l: " << l << " u: " << u  << " pivot: " << pivot << std::endl << std::flush;
    v = a[pivot];
    i = l - 1;
    j = u + 1;

    while(true) {
      do 
	i++;
      while(a[i] < v && i <= u);

      do
	j--;
      while(a[j] > v);

      if (i >= j) {
	return j;
      }

      //std::cout << "New i: " << i << " j: " << j << std::endl << std::flush;
      temp=a[i];
      a[i]=a[j];
      a[j]=temp;
    }
}

int partition(int a[], int l, int u)
{
    int i,j;
    int v, temp;
    //int pivot = l + (u-l) / 2;
    int pivot = l;
    //std::cout << "l: " << l << " u: " << u  << " pivot: " << pivot << std::endl;
    //v=a[l + (u-l) / 2];
    v=a[pivot];
    i=l;
    j=u+1;
    do
    {
        do
            i++;
        while (a[i] < v && i <= u);

        do
            j--;
        while (v < a[j]);
    	
        if(i < j)
        {
            temp=a[i];
            a[i]=a[j];
            a[j]=temp;
        }
    } while (i < j);
    
    a[l]=a[j];
    a[j]=v;
    
    return j;
}

/**
 * Find and return the index of pivot element.
 * @param a - The array.
 * @param first - The start of the sequence.
 * @param last - The end of the sequence.
 * @return - the pivot element
*/
int pivot(int first, int last, int *a) 
{
    int  p = first;
    int pivotElement = a[first];

    for(int i = first+1 ; i <= last ; i++)
    {
        /* If you want to sort the list in the other order, change "<=" to ">" */
        if(a[i] <= pivotElement)
        {
            p++;
            swap(a[i], a[p]);
            //swap(i, p, a);
        }
    }

    //std::cout << "p: " << p <<  " 1st: " << first << std::endl;
    swap(a[p], a[first]);
    //swap(p, first, a);

    return p;
}

/**
 * Swap the parameters.
 * @param a - The first parameter.
 * @param b - The second parameter.
*/
void swap(int& a, int& b)
//void swap(int a, int b, int *arr )
{
    //std::cout << "a: " << a <<  " b: " << b << std::endl;
    int temp = a;
    a = b;
    b = temp;
}
