#!/bin/bash

#export PYTHONPATH=~/Documents/ice-personal:
#icepath=/home/thiago/Documents/ice-personal/bin
#pipspath=/home/thiago/Documents/pips_partile/MYPIPS
pipspath=/home/tteixei2/pipsPy3/MYPIPS
#intelpath=/opt/intel/compilers_and_libraries/linux
#arch="intel64"
compoptspath=/home/tteixei2/uiuc-compiler-opts/install

export ICE_CMPOPTS_PATH=$compoptspath
#export COMPILERVARS_ARCHITECTURE=$arch
#. ${intelpath}/bin/compilervars.sh

source /opt/intel/oneapi/setvars.sh --config="../../common/.intel_config"
export LD_LIBRARY_PATH=${intelpath}/lib/intel64:$LD_LIBRARY_PATH
#export LD_LIBRARY_PATH=/usr/lib/jvm/java-8-oracle/jre/lib/amd64/server:${LD_LIBRARY_PATH}
#export PATH=${icepath}:$PATH
. ${pipspath}/pipsrc.sh

# Necessary for Pips when using gcc 7 or 8
export PIPS_CPP="gcc -std=c99 -E -C -ffreestanding"
export PIPS_CC="gcc -std=c99 -ffreestanding -c -o /dev/null"

