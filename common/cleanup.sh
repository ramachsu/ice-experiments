#!/bin/bash

rm ice.log;
make clean;
mv ${SRC}.c.bkp ${SRC}.c
rm -rf pips-result-*;
rm -rf .pips-result-*;
rm -rf opentuner.db
rm -rf opentuner.log
