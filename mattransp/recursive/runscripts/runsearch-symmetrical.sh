#!/usr/bin/env bash

. ./runloop-base.sh

nruns=`seq 1` #3
ntests=10 #20000000000
cfile=mattransp.cpp
baseuplimit=10 #10
incuplimit=10
basestopaf=10 #25
incstopaf=10
locusfile=mattransp-vec-sym.locus # mattransp-cacheobliv.locus #
#locusfile=mattransp-cacheobliv.locus #
approxvalues=100
searchtools="ice-locus-opentuner.py" #ice-locus-hyperopt.py
#
inpvalues="`seq 1024 1024 8192`" #"`seq 1024 1024 8192`"
#inpvalues="`seq 1024 1024 1024`"

search_loop_body_pre(){
    echo Starting symmetrical search_loop_body_pre...
    xdebug="" #"--debug" #
    xparam=" --ntests ${ntests} " #"--timeout --upper-limit ${uplimit} --stop-after $stopaf " #"--exprec --approxexprec $approx --saveexprec"
    stopv=32 #$(( inp / 32 ))

    export ICELOCUS_MATSHAPE_M=$inp #512 #1024 #
    export ICELOCUS_MATSHAPE_N=$inp
    ##export ICELOCUS_CXX=g++ #clang++-8  #icpc #
    ##export ICELOCUS_CC=gcc #clang-8 #icc #
    export ICELOCUS_MTRANSP_STOP=$stopv 

    echo Ending symmetrical search_loop_body_pre
}

search_loop
