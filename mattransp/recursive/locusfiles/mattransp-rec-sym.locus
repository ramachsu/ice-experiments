matshapestrM = getenv("ICELOCUS_MATSHAPE_M", "1024");
matshapestrN = getenv("ICELOCUS_MATSHAPE_N", "1024");
melem = int(matshapestrM);
nelem = int(matshapestrN);

stop = int(getenv("ICELOCUS_MTRANSP_STOP", "32"));
mstop = int(stop);
nstop = int(stop);

buffername="gemmreplacement.txt";
cxx = getenv("ICELOCUS_CXX", "g++"); #"gcc";#"xlc";
cc = getenv("ICELOCUS_CC", "gcc"); #"gcc";#"xlc";

useomp = getenv("OMP_NUM_THREADS",False);

Search {
    #prebuildcmd = "";
    measureorig = False;
    buildcmd = "make CXX="+cxx+" CC="+cc+" USERDEFS='-DM="+matshapestrM+" -DN="+matshapestrN+"' clean mattransp.ice.exe";
    runcmd = "./mattransp.ice.exe";
    buildorig = "make CXX="+cxx+" CC="+cc+" USERDEFS='-DM="+matshapestrM+" -DN="+matshapestrN+"' clean orig";
    runorig = "./orig";
    checkcmd = "diff -q ./answer-mattransp-"+matshapestrN+"-"+matshapestrM+".txt ./orig-answer-mattransp-"+matshapestrN+"-"+matshapestrM+".txt";
}

OptSeq baseOptions(mS, mE, nS, nE) {
    mlen = mE-mS;
    #CallMethod.MtranspNaiveIJ(mS=mS, mE=mE, nS=nS, nE=nE, buffer=buffername);
    if (mlen % 8) == 0 {
      CallMethod.MtranspNaiveIJRegTileI8(mS=mS, mE=mE, nS=nS, nE=nE, omp=useomp, buffer=buffername);
    } elif (mlen % 4) == 0 {
      CallMethod.MtranspNaiveIJRegTileI4(mS=mS, mE=mE, nS=nS, nE=nE, omp=useomp, buffer=buffername);
    } else {
      CallMethod.MtranspNaiveIJ(mS=mS, mE=mE, nS=nS, nE=nE, omp=useomp, buffer=buffername);
    }
}

OptSeq recMtranspVector(mS, mE, nS, nE, veclvl, lvl) {
  nextlvl = lvl + 1;
  mlen = mE-mS;
  nlen = nE-nS;

  if veclvl[lvl] == "splitN" {
    pivn = nS+((nE-nS) // 2);
    if (pivn > nS) && (pivn < nE) {
      recMtranspVector(mS, mE, nS, pivn, veclvl, nextlvl);
      recMtranspVector(mS, mE, pivn, nE, veclvl, nextlvl);
    } else {
      RecursionStopped();
    }
  } elif veclvl[lvl] == "splitM" {
    pivm = mS+((mE-mS) // 2);
    if (pivm > mS) && (pivm < mE) {
      recMtranspVector(mS,   pivm, nS, nE, veclvl, nextlvl);
      recMtranspVector(pivm, mE,   nS, nE, veclvl, nextlvl);
    } else {
      RecursionStopped();
    }
  } elif veclvl[lvl] == "leaf" {
#CallMethod.MtranspNaiveIJ(mS=mS, mE=mE, nS=nS, nE=nE, buffer=buffername);
    baseOptions(mS, mE, nS, nE);
  } else {
    RaiseProblem();
  }
}

OptSeq recMtranspSym(mS, mE, nS, nE, veclvl, lvl) {
  nextlvl = lvl + 1;
  nlen = nE-nS;
  mlen = mE-mS;
  if (mlen > mstop) && (nlen > nstop) {
    {
      if nlen > nstop {
        pivn = nS+(nlen // 2);
        veclvl = veclvl + ["splitN"];
        recMtranspSym   (mS, mE, nS, pivn, veclvl, nextlvl);
        recMtranspVector(mS, mE, pivn, nE, veclvl, nextlvl);
      } else {
        RecursionStopped();
      }
    } OR {
      if mlen > mstop {
        pivm = mS+(mlen // 2);
        veclvl = veclvl + ["splitM"];
        recMtranspSym   (mS,   pivm, nS, nE, veclvl, nextlvl);
        recMtranspVector(pivm, mE,   nS, nE, veclvl, nextlvl);
      } else {
        RecursionStopped();
      }
    } OR {
      veclvl = veclvl + ["leaf"];
      baseOptions(mS, mE, nS, nE);
    }
  } else {
    RecursionStopped();
  }
}

CodeReg transp {
    CallMethod.CreateFile(buffer=buffername);

    #maxlvls = log(melem/stop);
    # All must have the same stop value.
    # this is because of the conditions (e.g., klen >= maxmn).
    # it stops the recursion despite the values of the recursion are much lower.
    # check paper Cache-Oblivious Algorithms. MATTEO FRIGO, CHARLES E. LEISERSON, et al.
    # https://dl.acm.org/doi/pdf/10.1145/2071379.2071383 
    #mstop = poweroftwo((melem // 8) .. melem); #enum(2048, 1024, 512, 256, 128, 64);
    #mstop = enum((melem // 8), (melem // 4), (melem // 2), melem); #enum(2048, 1024, 512, 256, 128, 64);
    #mstop = enum((melem // 16), (melem // 8), (melem // 4), (melem // 2));
    #nstop = enum((melem // 16), (melem // 8), (melem // 4), (melem // 2)); #mstop;

    #recMatmulBug(0, melem, 0, nelem, 0, kelem, "C", tmpcnameSuf);
    #pragma expandrecursion inline
    if useomp != False {
        CallMethod.OMPprolog(buffer=buffername);
    }
    recMtranspSym(0, melem, 0, nelem, [], 0);
    if useomp != False {
        CallMethod.OMPepilog(buffer=buffername);
    }
    Builtins.Altdesc(source=buffername);
}
