## Evalutation of GEMM patterns ##


This code is based on the plutobench directory.

```
M>>N, N=K={2,5,10}
M = {400 ... 4000 by 100}
```

### Results
To get results from the Locus database:
```
#Activate the python environment that contains the ice-locus-expchart installed 
source ~/pyvirtenvs/envpy3.6-ice/bin/activate
./matmul-genCSVfromDB.py -i locus.db/sadr.db -l matmul.locus > locus-sadr-res.csv
``` 

### Locus DB useful scripts

They belong to `ice-locus-expchart`:

1. `expchart-showDBInfo.py -i locus.db/sadr.db`
2. `expchart-getDBLocusfiles.py -i locus.db/sadr.db -l matmul.locus`

