#!/usr/bin/env python

from os import path
import argparse, sys, logging, statistics as stat

#print(f"Before loading dbutils from {__name__}")

from icelocusexpchart.common.charts.dbutils import (getDBBestVar, 
    getAllDBexpsOPT, printDBCfgs)

#print(f"After loading dbutils from {__name__}")
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console = logging.StreamHandler()
log.addHandler(console)

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Path to the db")
parser.add_argument('-l', type=str, required=True, nargs='+',
        help="Locus programs related info from DB.")
parser.add_argument('--inccfg', default=False, action='store_true',
        help="Add cfg DB id to the 1st field.")

args=parser.parse_args()

def genCSVfromDB(dbinp, locusfiles):
    #res_gen = getDBBestVar(dbinp, locusfiles, 
    res_gen = getAllDBexpsOPT(dbinp, locusfiles, 
            ['ICELOCUS_CC','ICELOCUS_MATSHAPE_M', 
                'ICELOCUS_MATSHAPE_N',
                'ICELOCUS_MATSHAPE_K',
                'ICELOCUS_OMP_NUMTH'])

    print(f"leg,comp,searchtool,numthreads,matshapeM,matshapeN,matshapeK,runid,time")

    if args.inccfg:
        printDBCfgs(dbinp, locusfiles)
    #

    # Store results for same shape
    shapeDict = {}

    # Variant and experiments loop
    for metainfo, var, exp, expvals in res_gen:
        #print(f"tmp: {type(tmp), {tmp}}")
        lfname = metainfo['lfname']
        comp = metainfo['ICELOCUS_CC']
        shapeM = metainfo['ICELOCUS_MATSHAPE_M'] 
        shapeN = metainfo['ICELOCUS_MATSHAPE_N'] 
        shapeK = metainfo['ICELOCUS_MATSHAPE_K'] 
        nth =  metainfo.get('ICELOCUS_OMP_NUMTH', '1')
        setool = metainfo['searchtool']

        if args.inccfg:
            lfname += "_"+str(var.configuration.id)
        #

        #descdict = {ev.desc: ev.metric for ev in expvals}
        #print(f"{lfname},{comp},{setool},{nth},{var.id},{shape},{shape},{shape},{descdict['Total']}")

        metric=expvals.metric
        if metric != float('inf'):
            #print(f"{lfname},{comp},{setool},{nth},{shapeM},{shapeN},{shapeK},{var.id},{metric}")
            shapeTuple = (lfname, comp, setool, nth, shapeM, shapeN, shapeK)
            if shapeTuple not in shapeDict:
                # create a dict that saves the run with the fastest median metric
                shapeDict[shapeTuple] = {}
            ##
            if var.id not in shapeDict[shapeTuple]:
                shapeDict[shapeTuple][var.id] = []
            ##
            shapeDict[shapeTuple][var.id].append(metric)
        else:
            print(f"Found inconsistent expval! Likely ok to skip. varid:"
                    f" {var.id} expval: {exp.id} metric: {metric}", file=sys.stderr)
        #
    #

    shapeDictMedian = {}
    # Get the median values of the runs of each shape
    for k,v in shapeDict.items():
        if k not in shapeDictMedian:
            shapeDictMedian[k] = []
        #
        # for each run get the median
        for k_run, v_run in v.items():
            #
            med = stat.median(v_run)
            shapeDictMedian[k].append((k_run,med))
        ##
    ##
    # Get best for each shape
    for k,v in shapeDictMedian.items():
        runid, min_ = min(v, key = lambda t: t[1])
        print(f"{','.join(v for v in k)},{runid},{min_}")
    ##
###

if __name__ == '__main__':
    genCSVfromDB(args.i, args.l)
#



