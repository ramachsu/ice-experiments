#!/bin/bash

die () {
	echo >&2 "$@"
	exit 1
}
ntests=100
if [ "$#" -eq 1 ];then ntests=${1}; fi #|| die "1 filename required -- input to be encrypted and the output will be input + .enc , $# provided

locusf=matmul.locus #mmc.locus
srcfile=matmul.c #mmc.c

export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_NUM_THREADS=1
export ICELOCUS_OMP_PLACES=$OMP_PLACES
export ICELOCUS_OMP_PROCBIND=$OMP_PROC_BIND
export ICELOCUS_OMP_NUMTH=$OMP_NUM_THREADS
export ICELOCUS_CC=icc
export ICELOCUS_CXX=icpc

beta="0.335009"
alpha="-0.364523"

#../../common/bosshowtorun.sh  -s -n ${ntests} -t ${locusf} -f ${srcfile}
#for M in 400 800 1200 1600 2000 2400 2800 3200 3600 4000; do
for M in 400 800 1200 1600 2000 2400 2800 3200 3600 4000; do
  for N in 2 5 10; do
    #for K in 2 10; do
    K=$N
    export ICELOCUS_MATSHAPE_M=$M
    export ICELOCUS_MATSHAPE_N=$N
    export ICELOCUS_MATSHAPE_K=$K
    ./sedchange.sh $M $N $K matmul.base.c matmul.c;
    origoutf="output_orig_matmul_${M}_${N}_${K}.txt"
    if [ ! -f $origoutf ]; then
        make CC=$ICELOCUS_CC CXX=$ICELOCUS_CXX clean orig
        ./orig $beta $alpha $origoutf
    fi
    ice-locus-exhaustive.py --search -n ${ntests} -t ${locusf} -f ${srcfile} --tfunc mytiming.py:getTimingMatMul -o suffix -u .opt
    #done
  done
done
