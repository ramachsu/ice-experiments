//#include "../utils/utils.h"
#include <mkl.h>
#include "utils.h"
#include "mysecond.h"

int main(int argc, char *argv[]) {
    // Matrix dimensions (global)
    int m, n, k, iter, iterations = 5;

    // Matrices
    double *A, *B, *C;
    double beta, alpha;

    // Timing variables
    struct timespec t_start, t_end;

    /* Get command line arguments */
    if (argc < 4) {
        fprintf(stderr, "Usage: %s <m> <n> <k> [<beta>] [<alpha>]\n", argv[0]);
        exit(1);
    } else {
        m = atoi(argv[1]);
        n = atoi(argv[2]);
        k = atoi(argv[3]);
        if (argc == 6) {
            beta = atof(argv[4]);
            alpha = atof(argv[5]);
        }
    }

    double *t_it = (double *) malloc(iterations * sizeof(double));
    if (!t_it){printf("Could not allocate t_it.\n"); exit(1);}

    // Allocate matrices
    A = create(m, k);
    B = create(k, n);
    C = create(m, n);

    // Initialize matrices
    initGlobal(A, m, k);
    initGlobal(B, k, n);
    initZero(C, m, n);

    //printMatrix(A, m, k);
    //printMatrix(B, k, n);

    // Matrix multiplication!
    for (iter = 0; iter < iterations; iter++) {
       mygettime(&t_start);
       cblas_dgemm(CblasColMajor,
                CblasNoTrans, CblasNoTrans,
                m, n, k,
                alpha,
                A, m,
                B, k,
                beta,
                C, m);
       mygettime(&t_end);
       t_it[iter] = mydifftimems(&t_start, &t_end);
    }
    //printMatrix(C, m, n);
    double gflopA = (2.0*m*n*k)*1E-9;
    double gflopB = 2.0*((m*n*k)+(m*n))*1E-9;
    printf("MKL numthreads= %d shape= %d %d %d beta= %lf alpha= %lf | Iterations %d Time(milisec) GFlopsA GFlopsB ", mkl_get_max_threads(), m, n, k, beta, alpha, iterations);
    for(iter = 0; iter < iterations; iter++) {
        printf("%7.5lf %.5e %.5e ",t_it[iter], t_it[iter]/gflopA, t_it[iter]/gflopB);
    }
    printf("\n");
    return 0;
}
