#!/bin/bash

matshape=2048
ntests=1000
stopafter=60
rm -rvf opentuner.db opentuner.log
ice-locus-opentuner.py --stop-after $stopafter --timeout -t matmul.locus -f matmul.${matshape}.c --tfunc mytiming.py:getTimingMatMul -o suffix -u '.ice' --search --ntests ${ntests}

