#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
//#include <assert.h>
#include <unistd.h>
#include <sys/time.h>
#include <float.h>
#include <pips_runtime.h>

#ifndef M
#define M 2048
#endif

#ifndef N
#define N 2048
#endif

#ifndef K
#define K 2048
#endif

#define alpha 1
#define beta 1

#define BUFNAMESIZE 32

//#pragma declarations
double A[M][K];
double B[K][N];
double C[M][N];
//#pragma enddeclarations
//#ifdef TIME
#define IF_TIME(foo) foo;
//#else
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            A[i][j] = (i + j);
            B[i][j] = (double)(i*j);
            C[i][j] = 0.0;
        }
    }
}

void print_array()
{
    int i, j;
    char bufname[32];
    snprintf(bufname, BUFNAMESIZE, "answer-matmul-%d.txt", N);
    FILE *fout = fopen(bufname,"w");
    if(!fout){
          fprintf(stderr,"ERROR! Could not open %s to output results!\n", bufname);
          exit(1);
    }

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            fprintf(fout, "%lf ", C[i][j]);
            if (j%80 == 79) fprintf(fout, "\n");
        }
        fprintf(fout, "\n");
    }

    fclose(fout);
}


double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

double t_start, t_end;

int main()
{
    int i, j, k, iterations = 5;

    double min=DBL_MAX, max=DBL_MIN, avg, t_all = 0.0, t_it;

    init_array();

#ifdef PERFCTR
    PERF_INIT; 
#endif
    for (int it = 0; it < iterations; it++) {
       IF_TIME(t_start = rtclock());

#pragma @ICE loop=matmul
       for(i=0; i<M; i++)
          for(j=0; j<N; j++)  
             for(k=0; k<K; k++)
                C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];

       IF_TIME(t_end = rtclock());
       IF_TIME(t_it = t_end - t_start);

       IF_TIME(if (t_it > 0 && t_it < min) min = t_it);
       IF_TIME(if (t_it > 0 && t_it > max) max = t_it);
       IF_TIME(t_all += t_it);

       if (it== 0 && fopen(".test", "r")) {
#ifdef MPI
         if (my_rank == 0) {
          print_array();
         }
#else
         print_array();
#endif
       }
    }
    IF_TIME(avg = t_all/iterations);

    IF_TIME(printf("Matrixsize= %d %d %d | Time(ms) min= %7.5lf max= %7.5lf avg= %7.5lf | Iterations %d\n", M, N, K, min*1.0e3, max*1.0e3, avg*1.0e3, iterations));

#ifdef PERFCTR
    PERF_EXIT; 
#endif



  return 0;
}
