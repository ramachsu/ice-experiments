#!/usr/bin/env bash

export ICELOCUS_GEMM_SHAPE=128 #2048
export ICELOCUS_CC="icc" #"gcc" # !!! Change the locus too!
export ICELOCUS_CXX="icpc" #"gcc" # !!! Change the locus too!
#export EXPDIR=exper-`date +%Y%m%d-%H%M`
ntests=24  #20000000000
stopaf=120 #360 #60 # in minutes
nruns=10 #per backend
uplimit=50 # in seconds
cfile=matmul.${ICELOCUS_GEMM_SHAPE}.c
locusvs="V2perm" #"lvl2perm" #"lvl3perm" #"lvl2perm" # "lvl1" 
initpnt="3lvl" #"" #
#version=${locusvs}"-"${ICELOCUS_CC}"-w"${initpnt}"init" #"V3" # 
xparams="--debug"

#algos="rand tpe anneal mixA mixB" # atpe not working
algos="mixC" # atpe not working

declare -A mixdsc
mixdsc=(["rand"]="None" ["tpe"]="None" 
		["anneal"]="None" ["mixA"]=".25:rand:.75:tpe" 
		["mixB"]=".25:rand:.75:anneal" 
		["mixC"]=".25:rand:.50:anneal:0.25:tpe")

pyversion=`python --version | awk '{print $2}'`
hyoptvs=`pip show hyperopt | grep Version | awk '{print $2}'`
version=py${pyversion}-hyp${hyoptvs}-${locusvs}"-"${ICELOCUS_CC}"-w"${initpnt}"init" #"V3" # 
#testOR.locus #
locusfile=matmul-${locusvs}-w${initpnt}init.locus #matmul-V2-winit.locus #test-list.locus #test-bug.locus #

