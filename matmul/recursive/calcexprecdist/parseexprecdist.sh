#!/usr/bin/env bash

# Parse ice-loucus logs files of only expanding the recursion according to a approximation.
# logs generated by ice-experiments/matmul/recursive/savedexprec/genexprec.sh

inputfile=$1
echo "Approx,expanded,totalcalls,time"
grep "tress\|Expansion of" $inputfile | awk 'NR%2{printf("%s,",$8);next}{print $11 "," $14}' | awk -F '[/(]' '{print $1 "," $2 $3}'
