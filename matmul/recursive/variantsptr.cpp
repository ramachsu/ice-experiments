#include "variantsptr.h"

/////////////////////
//
// I-J-K
//
/////////////////////

void naiveIJK(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIJK
for(i = mS; i < mE; i++) {
  for(j = nS; j < nE; j++) {
     for(k = pS; k < pE; k++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
    }
  }
}
#pragma @ICE endloop

//C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];
}

void naiveIJK(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIJK
for(i = 0; i < mlen; i++) {
  for(j = 0; j < nlen; j++) {
    for(k = 0; k < klen; k++) {
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j,ldb)];
    }
  }
}
#pragma @ICE endloop
}

void naiveIJKRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIJK
for(i = mS; i < mE; i++) {
  for(j = nS; j < nE; j+=4) {
     for(k = pS; k < pE; k++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

//C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];
}

void naiveIJKRegTileJ8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIJK
for(i = mS; i < mE; i++) {
  for(j = nS; j < nE; j+=8) {
     for(k = pS; k < pE; k++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];
	    C[ind(i,j+4,lenN)] = beta*C[ind(i,j+4,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+4,lenN)];
	    C[ind(i,j+5,lenN)] = beta*C[ind(i,j+5,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+5,lenN)];
	    C[ind(i,j+6,lenN)] = beta*C[ind(i,j+6,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+6,lenN)];
	    C[ind(i,j+7,lenN)] = beta*C[ind(i,j+7,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+7,lenN)];
    }
  }
}
#pragma @ICE endloop

//C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];
}

/////////////////////
//
// I-K-J
//
/////////////////////

void naiveIKJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k++) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJ(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i++) {
  for(k = 0; k < klen; k++) {
    for(j = 0; j < nlen; j++) {
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j,ldb)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileJ4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k++) {
    for(j = nS; j < nE; j+=4) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileJ4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i++) {
  for(k = 0; k < klen; k++) {
    for(j = 0; j < nlen; j+=4) {
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j+3,ldb)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=4) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i++) {
  for(k = 0; k < klen; k+=4) {
    for(j = 0; j < nlen; j++) {
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j,ldb)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=8) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK8(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i++) {
  for(k = 0; k < klen; k+=8) {
    for(j = 0; j < nlen; j++) {
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+7,lda)] * B[ind(k+7,j,ldb)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK4J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=4) {
    for(j = nS; j < nE; j+=4) {
	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k  ,lenK)] * B[ind(k,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK4J4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i++) {
  for(k = 0; k < klen; k+=4) {
    for(j = 0; j < nlen; j+=4) {
	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k  ,lda)] * B[ind(k,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k  ,lda)] * B[ind(k,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k  ,lda)] * B[ind(k,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k  ,lda)] * B[ind(k,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j+3,ldb)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK8J4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i++) {
  for(k = pS; k < pE; k+=8) {
    for(j = nS; j < nE; j+=4) {
	    C[ind(i,j,lenN)]   = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j+3,lenN)];

	    C[ind(i,j  ,lenN)] = beta*C[ind(i,j  ,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j  ,lenN)];
	    C[ind(i,j+1,lenN)] = beta*C[ind(i,j+1,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j+1,lenN)];
	    C[ind(i,j+2,lenN)] = beta*C[ind(i,j+2,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j+2,lenN)];
	    C[ind(i,j+3,lenN)] = beta*C[ind(i,j+3,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j+3,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileK8J4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i++) {
  for(k = 0; k < klen; k+=8) {
    for(j = 0; j < nlen; j+=4) {
	    C[ind(i,j,ldc)]   = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+4,lda)] * B[ind(k+4,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+4,lda)] * B[ind(k+4,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+4,lda)] * B[ind(k+4,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+4,lda)] * B[ind(k+4,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+5,lda)] * B[ind(k+5,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+5,lda)] * B[ind(k+5,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+5,lda)] * B[ind(k+5,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+5,lda)] * B[ind(k+5,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+6,lda)] * B[ind(k+6,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+6,lda)] * B[ind(k+6,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+6,lda)] * B[ind(k+6,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+6,lda)] * B[ind(k+6,j+3,ldb)];

	    C[ind(i,j  ,ldc)] = beta*C[ind(i,j  ,ldc)] + alpha*A[ind(i,k+7,lda)] * B[ind(k+7,j  ,ldb)];
	    C[ind(i,j+1,ldc)] = beta*C[ind(i,j+1,ldc)] + alpha*A[ind(i,k+7,lda)] * B[ind(k+7,j+1,ldb)];
	    C[ind(i,j+2,ldc)] = beta*C[ind(i,j+2,ldc)] + alpha*A[ind(i,k+7,lda)] * B[ind(k+7,j+2,ldb)];
	    C[ind(i,j+3,ldc)] = beta*C[ind(i,j+3,ldc)] + alpha*A[ind(i,k+7,lda)] * B[ind(k+7,j+3,ldb)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileI4K4(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i+=4) {
  for(k = pS; k < pE; k+=4) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];

	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+3,lenK)] * B[ind(k+3,j,lenN)];

	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+3,lenK)] * B[ind(k+3,j,lenN)];

	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+3,lenK)] * B[ind(k+3,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

// This new version follows BLAS interface
void naiveIKJRegTileI4K4(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i+=4) {
  for(k = 0; k < klen; k+=4) {
    for(j = 0; j < nlen; j++) {
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j,ldb)];

	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+3,lda)] * B[ind(k+3,j,ldb)];

	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+3,lda)] * B[ind(k+3,j,ldb)];

	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+3,lda)] * B[ind(k+3,j,ldb)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileI8K8(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   ITERC lenN, ITERC lenK,
	   MATTYPE A, MATTYPE B, MATTYPE C){

ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = mS; i < mE; i+=8) {
  for(k = pS; k < pE; k+=8) {
    for(j = nS; j < nE; j++) {
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i,j,lenN)] = beta*C[ind(i,j,lenN)] + alpha*A[ind(i,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+1,j,lenN)] = beta*C[ind(i+1,j,lenN)] + alpha*A[ind(i+1,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+2,j,lenN)] = beta*C[ind(i+2,j,lenN)] + alpha*A[ind(i+2,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+3,j,lenN)] = beta*C[ind(i+3,j,lenN)] + alpha*A[ind(i+3,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+4,j,lenN)] = beta*C[ind(i+4,j,lenN)] + alpha*A[ind(i+4,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+5,j,lenN)] = beta*C[ind(i+5,j,lenN)] + alpha*A[ind(i+5,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+6,j,lenN)] = beta*C[ind(i+6,j,lenN)] + alpha*A[ind(i+6,k+7,lenK)] * B[ind(k+7,j,lenN)];

	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k,lenK)] * B[ind(k,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+1,lenK)] * B[ind(k+1,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+2,lenK)] * B[ind(k+2,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+3,lenK)] * B[ind(k+3,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+4,lenK)] * B[ind(k+4,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+5,lenK)] * B[ind(k+5,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+6,lenK)] * B[ind(k+6,j,lenN)];
	    C[ind(i+7,j,lenN)] = beta*C[ind(i+7,j,lenN)] + alpha*A[ind(i+7,k+7,lenK)] * B[ind(k+7,j,lenN)];
    }
  }
}
#pragma @ICE endloop

}

void naiveIKJRegTileI8K8(ITERC mlen, ITERC nlen, ITERC klen, 
                        MATTYPE A, ITERC lda,
                        MATTYPE B, ITERC ldb,
                        MATTYPE C, ITERC ldc)
{
ITER i, j, k;

#pragma @ICE loop=kernelIKJ
for(i = 0; i < mlen; i+=8) {
  for(k = 0; k < klen; k+=8) {
    for(j = 0; j < nlen; j++) {
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i,j,ldc)] = beta*C[ind(i,j,ldc)] + alpha*A[ind(i,k+7,lda)] * B[ind(k+7,j,ldb)];

	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i+1,j,ldc)] = beta*C[ind(i+1,j,ldc)] + alpha*A[ind(i+1,k+7,lda)] * B[ind(k+7,j,ldb)];

	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i+2,j,ldc)] = beta*C[ind(i+2,j,ldc)] + alpha*A[ind(i+2,k+7,lda)] * B[ind(k+7,j,ldb)];

	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i+3,j,ldc)] = beta*C[ind(i+3,j,ldc)] + alpha*A[ind(i+3,k+7,lda)] * B[ind(k+7,j,ldb)];

	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i+4,j,ldc)] = beta*C[ind(i+4,j,ldc)] + alpha*A[ind(i+4,k+7,lda)] * B[ind(k+7,j,ldb)];

	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i+5,j,ldc)] = beta*C[ind(i+5,j,ldc)] + alpha*A[ind(i+5,k+7,lda)] * B[ind(k+7,j,ldb)];

	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i+6,j,ldc)] = beta*C[ind(i+6,j,ldc)] + alpha*A[ind(i+6,k+7,lda)] * B[ind(k+7,j,ldb)];

	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k,lda)] * B[ind(k,j,ldb)];
	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k+1,lda)] * B[ind(k+1,j,ldb)];
	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k+2,lda)] * B[ind(k+2,j,ldb)];
	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k+3,lda)] * B[ind(k+3,j,ldb)];
	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k+4,lda)] * B[ind(k+4,j,ldb)];
	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k+5,lda)] * B[ind(k+5,j,ldb)];
	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k+6,lda)] * B[ind(k+6,j,ldb)];
	    C[ind(i+7,j,ldc)] = beta*C[ind(i+7,j,ldc)] + alpha*A[ind(i+7,k+7,lda)] * B[ind(k+7,j,ldb)];
    }
  }
}
#pragma @ICE endloop

}


MATTYPE allocaTmp(ITERC mS, ITERC mE, ITERC nS, ITERC nE) {
	MATTYPE tmp = (double *) calloc((mE-mS)*(nE-nS),sizeof(double));
	return tmp;
}

void MatrixAddOld(ITERC mS, ITERC mE, ITERC nS, ITERC nE, ITERC lenN, MATTYPE C, MATTYPE tmp)
{
ITER i, j;
for(i = mS; i < mE; i++)
    for(j = nS; j < nE; j++)
	    C[ind(i,j,lenN)] += tmp[ind(i,j,lenN)];
}

void MatrixAdd(ITERC mlen, ITERC nlen, MATTYPE C, ITERC LDC, MATTYPE T, ITERC LDT)
{
ITER i, j;
for(i = 0; i < mlen; i++)
    for(j = 0; j < nlen; j++)
	    C[ind(i,j,LDC)] += T[ind(i,j,LDT)];
}
