#ifndef __VARIANTS_H__
#define __VARIANTS_H__

#include "globals.tmp.h"

void naiveIJK(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   double A[M][K], double B[K][N], double C[M][N]);

void naiveIKJ(ITERC mS, ITERC mE,
	   ITERC nS, ITERC nE,
	   ITERC pS, ITERC pE,
	   double A[M][K], double B[K][N], double C[M][N]);

#endif

