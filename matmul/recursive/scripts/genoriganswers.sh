#!/usr/bin/env bash

#set -o xtrace

#start=5
#end=12
#for p in `seq $start $end`; do
    #pptwo=`echo "2^${p}" | bc`
    #echo Running $p $pptwo
#ccc="clang-9" #"icc"
#cxx="clang++-9" #"icpc"
complst="clang++-8,clang-8 icpc,icc g++,gcc"
#complst="clang++-8,clang-8"
#complst="icpc,icc" #g++,gcc"
#optflst="-xHost -ipo '-O3 -xHost'"
#declare -a optflst=("-O3 -xHost" "-O3 -ipo" "-xHost -ipo" "-O3 -xHost -ipo")
declare -a optflst=("max" "-O3" "-O2" "-O1")
shapes=1024 #`seq 512 512 4096`

for comps in $complst; do 
    cxx=${comps%,*}
    ccc=${comps#*,}
    $ccc --version
    $cxx --version
done

for p in $shapes; do
    for optf in "${optflst[@]}"; do
        for comps in $complst; do 
            cxx=${comps%,*}
            ccc=${comps#*,}

            pptwo=$p
            echo Running M= $pptwo N= $pptwo K= $pptwo $ccc $cxx $optf `date`
            xoptparam=""
            if [[ $optf != *max* ]]
            then
                #xoptparam="OPTFLAGS=\"${optf}\""
                xoptparam="${optf}"
                echo $xoptparam
                make CC=${ccc} CXX=${cxx} OPTFLAGS="$xoptparam" USERDEFS="-DM=${pptwo} -DN=${pptwo} -DK=${pptwo}" clean orig
            else
                make CC=${ccc} CXX=${cxx} USERDEFS="-DM=${pptwo} -DN=${pptwo} -DK=${pptwo}" clean orig
            fi
            #echo $xoptparam
            ./orig $p
            #mv answer-matmul-${pptwo}.txt orig-answer-matmul-${pptwo}.txt 
            #echo Done $p $pptwo
            echo Done M= $pptwo N= $pptwo K= $pptwo $ccc $cxx $optf `date`
        done
    done
done

