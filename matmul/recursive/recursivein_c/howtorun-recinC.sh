#!/usr/bin/env bash
nruns=1 #3
ntests=20000000000
#ntests=2 #15
cfile=matmulptr.cpp
uplimit=50
stopaf=220
locusfile=matmul-symmetrical.locus
#approxvalues="65 75 85" # 50 75 100
searchtools=ice-locus-opentuner.py #ice-locus-hyperopt.py #
#stoplist="128 64"
xdebug="" #"--debug"
xparam="" #"--exprec --approxexprec $approx --saveexprec"

declare -A mixdsc
mixdsc=(["rand"]="None" ["tpe"]="None" ["anneal"]="None" ["mixA"]=".25:rand:.75:tpe" ["mixB"]=".25:rand:.75:anneal" ["mixC"]=".25:rand:.50:anneal:0.25:tpe" ["mixD"]=".50:rand:.50:anneal" ["mixE"]=".75:rand:.25:anneal")


#for approx in $approxvalues; do

    for search in $searchtools; do 

#        for stopv in $stoplist; do

            for r in `seq $nruns`; do

                export ICELOCUS_MATSHAPE=2048 #512 #1024 #
                export ICELOCUS_CXX=icpc
                export ICELOCUS_CC=icc
                #export ICELOCUS_GEMM_STOP=$stopv #32

                echo Searching approx= $approx matshape= $ICELOCUS_MATSHAPE stop= $ICELOCUS_GEMM_STOP $search r= $r `date`


                sed -e "s/define M 256/define M ${ICELOCUS_MATSHAPE}/g" \
                    -e "s/define N 256/define N ${ICELOCUS_MATSHAPE}/g" \
                    -e "s/define K 256/define K ${ICELOCUS_MATSHAPE}/g" globals.h > globals.tmp.h;

                n=${ICELOCUS_MATSHAPE}
                if [ ! -f ./orig-answer-matmul-${n}.txt ]; then
                    echo "Generating orig answer..."
                    make clean orig
                    ./orig
                    mv answer-matmul-${n}.txt orig-answer-matmul-${n}.txt
                fi

                xparam=""

                if [[ $search == *hyperopt* ]]
                then
                    al="mixC"
                    xparam+="--hypalg mix --hypmixdsc ${mixdsc[${al}]}"
                fi

                rm -rvf opentuner.*

                $search --stop-after $stopaf --timeout --upper-limit ${uplimit} \
                    -t ${locusfile} -f ${cfile} \
                    --tfunc mytiming.py:getTiming \
                    -o suffix -u '.ice' --search \
                    --ntests ${ntests}  \
                    $xparam $xdebug

            done
#        done
	done
#done
