#!/usr/bin/env bash

#ntests=20000000000
export ICELOCUS_CXX=clang++-9 #g++ #icpc 
export ICELOCUS_CC=clang-9 #gcc #icc

ntests=100
cfile=matmulptr.cpp #variantsptr.c #
uplimit=15
stopaf=50
locusfile=matmul-handunroll.locus
lowerszp2=5
upperszp2=11
#rangepw2=`seq $lowerszp2 $upperszp2`
rangepw2="`seq 128 128 2048`"


