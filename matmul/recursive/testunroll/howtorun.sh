#!/usr/bin/env bash

. ./runparams.sh

runwhat="search" #"orig"

echo "Range 2^x:" $rangepw2

for p in $rangepw2; do
    n=$p #`echo "2^${p}"|bc`
    echo Searching $p $n uplimit= $uplimit stopaf= $stopaf  `date`

export ICELOCUS_MATSHAPE=$n #128 #512 #

sed -e "s/define M 256/define M ${ICELOCUS_MATSHAPE}/g" \
    -e "s/define N 256/define N ${ICELOCUS_MATSHAPE}/g" \
    -e "s/define K 256/define K ${ICELOCUS_MATSHAPE}/g" globals.h > globals.tmp.h;

if [ ! -f ./orig-answer-matmul-${n}.txt ]; then
	echo "Generating orig answer..."
	make clean orig
	./orig
	mv answer-matmul-${n}.txt orig-answer-matmul-${n}.txt
fi 

if [[ $runwhat == *orig* ]] 
then
    make CXX=$ICELOCUS_CXX CC=$ICELOCUS_CC clean orig
    ./orig
    #cat ipo_out.optrpt >> ${aMM}_ipo_out.optrpt
    mv ipo_out.optrpt orig_${ICELOCUS_MATSHAPE}_noprecise.optrpt
else
#ice-locus-opentuner.py --stop-after $stopaf --timeout --upper-limit ${uplimit} \
ice-locus-opentuner.py  \
    -t ${locusfile} -f ${cfile} \
    --tfunc mytiming.py:getTiming \
    -o suffix -u '.ice' --search \
    --ntests ${ntests} #\
    #--saveexprec --exprec 
fi

uplimit=$((uplimit + uplimit))
stopaf=$((stopaf + stopaf))
done

