#!/usr/bin/env bash
nruns=1 #3
ntests=20 #20000000000
#ntests=15
cfile=matmulptr.cpp
uplimit=10 #150 #30 #
incstopaf=10
stopaf=5 #25 #175 #
locusfile=matmul-extsymmetric.locus  #
#locusfile=matmul-cacheobliv.locus #
#approxvalues="65 75 85" # 50 75 100
searchtools="ice-locus-opentuner.py" #ice-locus-hyperopt.py
#stoplist="128 64"
shapes=`seq 512 512 4096`

declare -A mixdsc
mixdsc=(["rand"]="None" ["tpe"]="None" ["anneal"]="None" 
        ["mixA"]=".25:rand:.75:tpe" ["mixB"]=".25:rand:.75:anneal" 
        ["mixC"]=".25:rand:.50:anneal:0.25:tpe" ["mixD"]=".50:rand:.50:anneal" 
        ["mixE"]=".75:rand:.25:anneal")


search_func() {
    xdebug="" #"--debug" #
    xparam+="--timeout --upper-limit ${uplimit} --stop-after $stopaf " #"--ntests ${ntests}"  #"--exprec --approxexprec $approx --saveexprec"
    #echo xparam= $xparam
    #echo uplimit= $uplimit

    echo Searching approx= $approx matshape= $ICELOCUS_MATSHAPE stop= $ICELOCUS_GEMM_STOP $search r= $r `date`
    echo xparam= $xparam

    $search   \
        -t ${locusfile} -f ${cfile} \
        --tfunc mytiming.py:getTiming \
        -o suffix -u '.ice' --search \
        $xparam $xdebug
}

#for approx in $approxvalues; do

    for search in $searchtools; do 

#        for stopv in $stoplist; do
        for matshape in $shapes; do
        #for matshape in `seq 3584 512 4096`; do

            for r in `seq $nruns`; do

                export ICELOCUS_MATSHAPE=$matshape #512 #1024 #
                #export ICELOCUS_CXX=g++ #clang++-9 #icpc #
                #export ICELOCUS_CC=gcc #clang-9 #icc #
                #export ICELOCUS_GEMM_STOP=$stopv #32

                sed -e "s/define M 256/define M ${ICELOCUS_MATSHAPE}/g" \
                    -e "s/define N 256/define N ${ICELOCUS_MATSHAPE}/g" \
                    -e "s/define K 256/define K ${ICELOCUS_MATSHAPE}/g" globals.h > globals.tmp.h;

                n=${ICELOCUS_MATSHAPE}
                if [ ! -f ./orig-answer-matmul-${n}.txt ]; then
                    echo "Generating orig answer..."
                    make CXX=$ICELOCUS_CXX CC=$ICELOCUS_CC clean orig
                    ./orig
                    mv answer-matmul-${n}.txt orig-answer-matmul-${n}.txt
                fi

                xparam=""

                if [[ $search == *hyperopt* ]]
                then
                    alg="mix"
                    mixalg="mixC"
                    xparam+="--hypalg $alg --hypmixdsc ${mixdsc[${mixalg}]}"
                    #alg="rand"
                    #xparam+="--hypalg $alg"
                fi

                rm -rvf opentuner.*

                search_func

            done
            uplimit=$((uplimit + uplimit))
            stopaf=$((stopaf + incstopaf))
        done
    done
#done
