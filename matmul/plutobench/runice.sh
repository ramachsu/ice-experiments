#!/bin/bash

ntest=1000
#stopaf=1080 #18 hours
stopaf=288 #~5 hours
locus=matmul-power9-remote-init.locus
dim=4096
comp=xlc-16-1-1 #gcc-8-2-0 
init=f #true 
if [ "$init" = true ]; then
	initseedvar="--initseed"
	loginitvar="initseed"
else
	initseedvar=""
	loginitvar="random"
fi

rm -rf opentuner.*

ice-locus-opentuner.py -t $locus -f matmul.${dim}.c \
	-o suffix -u '.ice' --search --ntests ${ntest} \
	--stop-after ${stopaf} \
	--tfunc mytiming.py:getTimingMatMul \
	--timeout ${initseedvar} &> output-${dim}-p9-remote-${comp}-${loginitvar}-1thread-`date +%Y%m%d%H%M`.txt
