
#!/bin/bash
export OMP_PLACES=cores 
export OMP_PROC_BIND=close 

for nproc in 1 `seq 2 2 10`; do
./cleanup.sh
export OMP_NUM_THREADS=${nproc}
echo "Numthreads:" $OMP_NUM_THREADS
time ./howtosearch.sh 1000  &> output-${nproc}thread-`date +%Y%m%d%H%M`.txt
echo "End Numthreads:" $OMP_NUM_THREADS

done
