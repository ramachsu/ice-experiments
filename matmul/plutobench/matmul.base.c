#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
//#include <assert.h>
#include <unistd.h>
#include <sys/time.h>
#include <pips_runtime.h>

#ifndef M
#define M 2048
#endif

#ifndef N
#define N 2048
#endif

#ifndef K
#define K 2048
#endif

#define alpha 1
#define beta 1

//#pragma declarations
double A[M][K];
double B[K][N];
double C[M][N];
//#pragma enddeclarations
//#ifdef TIME
#define IF_TIME(foo) foo;
//#else
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            A[i][j] = (i + j);
            B[i][j] = (double)(i*j);
            C[i][j] = 0.0;
        }
    }
}

void print_array()
{
    int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            fprintf(stderr, "%lf ", C[i][j]);
            if (j%80 == 79) fprintf(stderr, "\n");
        }
        fprintf(stderr, "\n");
    }
}


double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

double t_start, t_end;

int main()
{
    int i, j, k;

    init_array();

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock());

//#pragma scop
#pragma @ICE loop=matmul
    for(i=0; i<M; i++)
       for(j=0; j<N; j++)  
          for(k=0; k<K; k++)
             C[i][j] = beta*C[i][j] + alpha*A[i][k] * B[k][j];
//#pragma endscop

    IF_TIME(t_end = rtclock());
    IF_TIME(printf("Matrix size = %d | Time = %7.5lf ms\n", N,(t_end - t_start)*1.0e3));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

  if (fopen(".test", "r")) {
#ifdef MPI
      if (my_rank == 0) {
          print_array();
      }
#else
          print_array();
#endif
  }

  return 0;
}
