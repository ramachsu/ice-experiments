#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse
import math
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
args=parser.parse_args()

def searchModeFilter(mode):
    if mode == "FFTW_ESTIMATE":
        return "Estimate"
    elif mode == "FFTW_MEASURE":
        return "Measure"
    elif mode == "FFTW_PATIENCE":
        return "Patience"
    elif mode == "FFTW_EXHAUST":
        return "Exhaust"
    elif mode == "FFTW_LOCUS":
        return "Locus"
    else:
        return "N/A"
##

def addRelValu(df):
    ref="FFTW_ESTIMATE"
    reffield_et = 'etmin'
    reffield_pl = 'pltime'

    gp= df.groupby('len')
    #print(gp)
    refpllst = []
    refexavglst = []
    for name, v in gp:
        #print("*******")
        #print("name:", name, "\nv:",v['pltime'], type(v['pltime']),'\n')
        #print("name:", name, "\nv:",v,'\n')
        refv = v.query('plstrg == "'+ref+'"')[reffield_pl]
        refv2 = v.query('plstrg == "'+ref+'"')[reffield_et]
        mys = pd.Series([refv.iloc[0]]*len(v[reffield_pl]))
        mys2 = pd.Series([refv2.iloc[0]]*len(v[reffield_et]))
        #d = v['pltime'] / mys.values
        #d2 = v['etavg'] / mys2.values
        #calc the speedup
        d = mys.values / v[reffield_pl]
        d2 = mys2.values / v[reffield_et]
        #print("d2:",d2)
        refpllst.append(d)
        refexavglst.append(d2)
    ##

    #print(data)
    df['refpl'] = pd.concat(refpllst)
    df['refet'] = pd.concat(refexavglst)
#endif

def chartbase(dfsrt, yfield='refet', pltobj=plt):
    step=2
    barlabels=["$2^{"+str(int(math.log2(v)))+"}$" for v in dfsrt.len.unique()]
    #barlabels=[str(v) for v in dfsrt.len.unique()]
    x = np.arange(step,step*(len(barlabels)+1),step)
    strgs = ['FFTW_ESTIMATE','FFTW_MEASURE','FFTW_PATIENCE','FFTW_EXHAUST','FFTW_LOCUS'] #dfsrt.plstrg.unique()
    print(f"{strgs}")
    width=step/(len(strgs)+1)  #0.4

    curwidth = -((len(strgs)-1)/2)
    for st in strgs:
        tmpdf = dfsrt[(dfsrt.plstrg == st)]
        #y = tmpdf.etavg
        #y = tmpdf.refetavg
        y = tmpdf[yfield]
        #print(f"{tmpdf}")
        #print(f"{x}")
        #print(f"w: {curwidth}")
        newlabel = searchModeFilter(st)
        pltobj.bar(x+curwidth*width, y, width, label=newlabel)
        #print("x:",x,"y:",y.to_csv())
        curwidth += 1
    ##

    pltobj.grid(b=True, which='both', axis='y', linestyle='-', alpha=0.2)
    #pltobj.title("FFT")
    #pltobj.errorbar(x, y, yerr=[y-ymin, ymax-y], label="QR", capsize=2, marker='.')
    #pltobj.xticks(x, barlabels)
    pltobj.set_xticks(x)
    pltobj.set_xticklabels(barlabels)
    pltobj.tick_params(axis='x', which='major', labelsize=12)
    #pltobj.tick_params(axis='x', which='major', labelsize=8,
    #                labelrotation=45.0)
    pltobj.set_xlabel("N")

    #pltobj.ylabel("Time (miliseconds)")
#enddef

def chartexectime(df, pltobj=plt):
    chartbase(df, pltobj=pltobj)
    pltobj.set_ylabel("Execution Time Speedup (over Estimate)")
###

def chartsearchtime(df, pltobj=plt):
    chartbase(df, yfield='stopaf', pltobj=pltobj)
    #lgd = pltobj.legend(loc='upper left', bbox_to_anchor=(1.0, 0.95), ncol=1), bbox_to_anchor=(-0.10, 1.10)
    lgd = pltobj.legend(loc='upper center', ncol=5)
    pltobj.set_ylabel("Planning Time (milisec)")
    pltobj.set_yscale('log')
    return lgd
###

def fullfig(df, figname='myplot.pdf'):
    nrows = 2 #1
    ncols = 1 #2
    #fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(15,5))
    fig, axes = plt.subplots(nrows, ncols, figsize=(9,12),
            constrained_layout=True)

    addRelValu(df)
    dfsrt = df.sort_values(['len',
        'plstrg','etavg']).drop_duplicates(['len','plstrg'])
    #dfsrt.drop(dfsrt[dfsrt.len > 4194304].index, inplace=True)
    dfsrt.drop(dfsrt[dfsrt.len > 1048576].index, inplace=True)

    #dfsrt[['len','plstrg','stopaf','refet']].T.to_csv('finaldata.csv', index=False, header=False)
    #dfsrt.to_csv('finaldata.csv', index=False, columns=['len','plstrg','stopaf','refet'])
    mydf = dfsrt[['len','plstrg','stopaf','refet']] #.to_csv('finaldata.csv', index=False, header=False)

    #for keystrgs, strgs in mydf['plstrg'].unique():
    print(",",','.join(map(str,mydf['len'].unique())))
    for keystrgs, strgs in mydf.groupby('plstrg'):
        #b = strgs.T['refet'].to_csv(index=False, header=False)
        bt = strgs.T
        print(keystrgs+",",bt.loc[['stopaf']].to_csv(index=False, header=False))
        #print(strg+",")
    #

    lgd = chartsearchtime(dfsrt, axes[0])
    chartexectime(dfsrt, axes[1])

    figname=args.o
    plt.savefig(figname)
                #bbox_extra_artists=(lgd,),
                #bbox_inches='tight')
    print(f"Saved chart in {figname} .")

###

df = pd.read_csv(args.i, comment="#", sep='\s+')
print(df)
print(f"Reading file {args.i} ...")

#chartexectime(df)
fullfig(df, figname=args.o)


