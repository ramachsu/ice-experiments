'''
@author: thiago
'''

import re, os, sys, platform, glob
from setuptools import setup, find_packages

#Requires python 3 from now
if sys.version_info[0] < 3 and sys.version_info[0] < 6:
    print("ATTENTION!! Python 3.6 required, this is Python {}.".format(platform.python_version()))
    exit(1)
#endif

PKG = "icelocusexpchart"
VERSIONFILE = os.path.join(PKG, "_version.py")
verstr = "unknown"
try:
    verstrline = open(VERSIONFILE, "rt").read()
except OSError:
    pass
else:
    VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
    mo = re.search(VSRE, verstrline, re.M)
    if mo:
        verstr = mo.group(1)
    else:
        raise RuntimeError("Unable to find version string in %s.".format(VERSIONFILE))
    #endif
#endif


required=[]
deplinks=[]
with open('requirements-py3.txt') as inpreq:
    lines = inpreq.read().splitlines()
    lines = [l.strip() for l in lines
                if l.strip() and not 
                l.strip().startswith('#')]
    required = [l for l in lines
                if not l.startswith("git+")]
    deplinks = [l for l in lines
                 if l.startswith("git+")]
#endwith

## Get the binaries on all these dirs
dirs = ('fft','.')
binaries = []
for d in dirs:
    binaries += glob.glob(PKG+'/'+d+'/bin/*.py')
##
####

print(binaries)
print(required)
print(deplinks)
setup(
    name='ice-locus-expchart',
    version=verstr,
    url='',
    license='MIT',
    author='Thiago S F X Teixeira',
    author_email='tteixei2@illinois.edu',
    description='A library for running ICE-Locus experiments and generating charts.',
    #long_description=read_md('README.md'),
    packages=find_packages(),
    include_package_data=True,
    package_data={
      '': ['*.g'],
    },
    #install_requires=['python_version>3']+required,
    install_requires=required,
    dependency_links=deplinks,
    #dependency_links=[
    #    "git+https://github.com/hyperopt/hyperopt.git@1785e4d5280695b6b1e83b21ccf9c32d6999d3a3"
    #],
    #entry_points={
      ##'console_scripts': [
        #'ice-locus = ice-locus.__main__:main']},
    scripts=binaries+[
    ],
)
