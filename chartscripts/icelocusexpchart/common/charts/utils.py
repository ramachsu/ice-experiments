#!/usr/bin/env python

import sys, argparse, logging

log = logging.getLogger(__name__)

linestyles=["-","--","-.",":","-","--","-.",":"]
markerstyles=[".","+","x",'s','o','^','1','v','2'"+",".","s","x"]
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
        'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
patterns = [ "|||" , '\\\\\\' , '///' , "+++" , "---", "...", "*","x", "o", "O" ]

def autolabel(rects, pltobj, prec=2, lim=None):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        #if lim is None or height > lim:
        vpos = height if lim is None or height < lim else 0.9*lim
        pltobj.text(rect.get_x() + rect.get_width()/2., vpos,
            #f'{int(height)}',
            #f'{round(height,2):.2f}',
            f'{round(height,prec)}' if prec > 0 else f'{int(round(height))}',
            ha='center', va='bottom', fontsize='x-small')
        #
    ##
###
