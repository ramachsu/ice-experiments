#!/usr/bin/env python

import numpy as np, pandas as pd
import argparse, math, itertools
import matplotlib.pyplot as plt
import math, os

from icelocusexpchart.common.charts.utils import (linestyles, markerstyles, 
    colors, autolabel, patterns)

import matplotlib as mpl

mpl.rcParams['hatch.linewidth'] = 0.1

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
parser.add_argument('--nlcol', type=int, required=False, help="number of columns"
        "in the legend.", default=1)
parser.add_argument("--chart", nargs='+', required=False,
        default=('time','speedup'),
        choices=('time','speedup'),
        help="Chart options: time and speedup")


args=parser.parse_args()

def plot_time_nth(dfinp):
    df = dfinp

    datanth = df.groupby('numthreads')
    #dataleg = df.groupby('leg')
    xlabel = "N"

    maxncols = 2 #3
    ncols = min(maxncols, len(datanth))
    nrows = int(math.ceil(len(datanth) / maxncols))
    print(f"nth: {len(datanth)} ncols: {ncols} nrows: {nrows}")
    fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(ncols*5,nrows*5), 
            constrained_layout=True)
    #fig, axes = plt.subplots(nrows, ncols)
    lgd = None
    sbidx = 0
    sbidy = 0
    sbid = 0

    dlegsty = {e: (l,m,c) for e,l,m,c in zip(df['leg'].unique(), linestyles, 
        markerstyles, colors) }

    for keynth, grpnth in datanth:
        #pltobj = axes[sbidy] if ncols > 1 else axes
        if ncols > 1 and nrows > 1:
            pltobj =  axes[sbidx][sbidy]
        elif ncols > 1:
            # just single line
            pltobj = axes[sbidy]
        else:
            #ncols == 1 and nrwos == 1
            pltobj = axes
        #

        linesty = itertools.cycle(linestyles)
        mksty = itertools.cycle(markerstyles)
        pltobj.grid(b=True, which='both', linestyle='-', alpha=0.2)

        ycsv = ""

        dataleg = grpnth.groupby('leg')
        for keyleg, grpleg in dataleg:

            print(f"==== {keynth} {keyleg} sbid {sbid} sbidx {sbidx} sbidy {sbidy} ====")

            #print(f"{grpleg}")

            variants = grpleg.groupby(['numthreads','run','matshape']).aggregate(
                    [('med',np.median), ('min', min), ('max', max)])
            #variants = grpleg.groupby(['numthreads','run','matshape'])
            #for kpp, gpp in variants:
            #    print(f"groupby: {gpp.dtypes} {kpp} {gpp}")

            #variants = variants.aggregate([('med', np.median)])
            #print(f"Vamos la: {variants}")

            variants.columns = variants.columns.map('_'.join)
            variants = variants.reset_index()
            #print(f"Variants 2: {type(variants)}\n{variants}")

            variants = variants.loc[variants.groupby(['matshape','numthreads']).totaltime_med.idxmin()]
            #print(f"Variants 3: {type(variants)}\n{variants.head()}")

            x = grpleg['matshape'].unique()
            y = variants.totaltime_med
            label = keyleg #grpleg['matshape'].unique()
            #print(keyleg, y)
            #pltobj.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')
            pltobj.plot(x, y, label=label, marker=dlegsty[label][1], 
                    color=dlegsty[label][2],
                    linestyle=dlegsty[label][0], fillstyle='none')
            #print(variants.transpose().loc[['totaltime_med']])
            ycsv += keyleg+","+variants.transpose().loc[['totaltime_med']].to_csv(header=False, index=False)
        ##

        if (sbidy == 0 and sbidx == 0) or (sbidx == 0 and ncols == 1):
            lgd = pltobj.legend(loc='upper left', ncol=args.nlcol)
        ##
        if sbidx % nrows == (nrows - 1):
            pltobj.set_xlabel(xlabel)
        #
        if sbidy == 0:
            pltobj.set_ylabel("Time (milisec)")
        #

        pltobj.spines['top'].set_visible(False)
        pltobj.spines['right'].set_visible(False)
        #pltobj.spines['left'].set_visible(False)
        #pltobj.spines['bottom'].set_color('#DDDDDD')

        ticks=grpnth['matshape'].unique()

        print("ycsv:")
        print(","+','.join(map(str,ticks)))
        print(ycsv)
        #pltobj.tick_params(axis='x', labelsize=10) #, labelrotation=40.0)
        pltobj.set_xticks(ticks)
        if keynth == 1:
            pltobj.set_title(str(keynth)+" thread")
        else:
            pltobj.set_title(str(keynth)+" threads")
        #
        #pltobj.set_yscale('log')
        #pltobj.set_xscale('log')
        sbid += 1
        sbidy = sbid % ncols
        sbidx = sbid // ncols
    ##

    #ticks=df['matshape'].unique()
    #plt.xticks(ticks)

    #plt.xlabel("N")
    #plt.ylabel("Time (sec)")
    #plt.legend(loc='upper left')
    name, ext = os.path.splitext(args.o)
    figname= name+"_time"+ext
    #fig.tight_layout()
    plt.savefig(figname)
                #bbox_extra_artists=(lgd,),
                #bbox_inches='tight')
    print(f"Saved chart in {figname} .")
###

def plot_time(dfinp):
    df = dfinp
    dataleg = df.groupby('leg')

    nrows = 1
    ncols = 1
    fig, axes = plt.subplots(nrows, ncols)
    lgd = None
    sbidx = 0

    for keyleg, grpleg in dataleg:
        pltobj = axes[sbidx] if ncols > 1 else axes
        linesty = itertools.cycle(linestyles)
        mksty = itertools.cycle(markerstyles)
        pltobj.grid(b=True, which='both', linestyle='-', alpha=0.2)

        x = grpleg['matshape'].unique()
        y = grpleg.groupby('matshape').median().time/1e3 # to seconds
        label = keyleg #grpleg['matshape'].unique()
        print(keyleg, y)
        pltobj.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')
        pltobj.grid(b=True, which='both', linestyle='-', alpha=0.2)
    ##
    ticks=df['matshape'].unique()
    plt.xticks(ticks)

    plt.xlabel("N")
    plt.ylabel("Time (sec)")
    plt.legend(loc='upper left')
    figname=args.o
    plt.savefig(figname, bbox_inches='tight')
    print(f"Saved chart in {figname} .")
###

def plot_speedup(dfinp, shape=2048, bars=True):
    df = dfinp[dfinp['matshape'] == shape] #.sort_values(['numthreads'], ascending=True)
    dataleg = df.groupby('leg')

    nrows = 1
    ncols = 1
    fig, axes = plt.subplots(nrows, ncols, #figsize=(ncols*5, nrows*4), 
            constrained_layout=True)
    pltobj = axes
    linesty = itertools.cycle(linestyles)
    mksty = itertools.cycle(markerstyles)

    baseref = 'Bisec'
    variants = df[(df['numthreads'] == 1) & (df['leg'] == 'Bisec')].groupby(
            ['run','matshape']).aggregate(
                    [('med',np.median), ('min',min), ('max',max)])
    variants.columns = variants.columns.map('_'.join)
    variants = variants.reset_index()

    numthreads = len(df['numthreads'].unique())
    baseline = variants.totaltime_med.repeat(numthreads).reset_index(drop=True)

    ycsv = ""

    nbars = len(df['leg'].unique())
    #barwidth = 1.0/nbars
    #curbarpos = -0.5+barwidth
    onemat = 1
    barwidth = (onemat*0.9)/nbars
    curbarpos = (-(nbars/2.0)*barwidth)+(barwidth/2.0)
    pat =itertools.cycle(patterns)
    col =itertools.cycle(colors)

    for keyleg, gprleg in dataleg:
        #get baseline
        #baseline = gprleg[gprleg['numthreads'] == 1]

        print(f"==== {keyleg} ====")

        # get the fastest baseline variant for this strategy
        variants = gprleg.groupby(['numthreads','run','matshape']).aggregate(
                [('med',np.median), ('min', min), ('max', max)])

        variants.columns = variants.columns.map('_'.join)
        variants = variants.reset_index()
        #print(f"Variants 2: {type(variants)}\n{variants}")

        variants = variants.loc[variants.groupby(['matshape','numthreads']).totaltime_med.idxmin()]
        #print(f"Variants 3: {type(variants)}\n{variants}")

        #baseline = variants[variants['numthreads'] == 1].totaltime_med.repeat(variants.shape[0]).reset_index(drop=True)
        #print(f"Base: {type(baseline)} {baseline}")

        x = gprleg['numthreads'].unique()
        #y = baseline.totaltime_med idiv(variants.totaltime_med)
        #y = (1.0 / variants.totaltime_med) * baseline.totaltime_med
        y = baseline.div(variants.totaltime_med.reset_index(drop=True))
        #print(f" var: {type(variants.totaltime_med)} {variants.totaltime_med}  y: {y}")
        label = keyleg

        if bars:
            rects = pltobj.bar(x+curbarpos, y, label=label, width=barwidth-0.05,
                    hatch=next(pat), color='white', linewidth=0.4, edgecolor=next(col))
            yupperb = 0.5
            autolabel(rects, pltobj, prec=2) #, yupperb)
            curbarpos += barwidth 
        else:
            pltobj.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')
        #

        pltobj.grid(b=True, which='major', axis='y', linestyle='-', alpha=0.2)
        #pltobj.annotate(f"{j:.2f}", xy=(i,j), xytext=(-10,10), textcoords='offset points')
        #

        #print('y',type(y))
        #ycsv += y.to_csv(header=False, index=True)
        ycsv += keyleg+","+','.join(map(str,y))+'\n'
    ##
    print("ycsv:")
    print(ycsv)

    pltobj.spines['top'].set_visible(False)
    pltobj.spines['right'].set_visible(False)
    pltobj.spines['left'].set_visible(False)
    #pltobj.spines['bottom'].set_color('#DDDDDD')
    pltobj.spines['bottom'].set_color('black')

    ticks=df['numthreads'].unique()
    pltobj.set_xticks(ticks)
    pltobj.tick_params(left=False)
    #plt.tick_params(axis='x', labelsize=8, labelrotation=40.0)

    pltobj.set_xlabel("Threads")
    pltobj.set_ylabel("Speedup")
    pltobj.set_yticks([1,2,3])
    pltobj.legend(loc='upper left', ncol=1, framealpha=0.4, frameon=True) #prop={'size':8}
    name, ext = os.path.splitext(args.o)
    figname= name+"_speedup"+ext
    fig.savefig(figname, bbox_inches='tight')
    print(f"Saved chart in {os.path.abspath(figname)} .")
###

df = pd.read_csv(args.i, comment="#")
print(df)
print(f"Reading file {args.i} ...")
#print(f"Plot selected {args.p}!")

print(f"Types {df.dtypes}\n{df.convert_dtypes().dtypes}")
df.sort_values(['numthreads','matshape'], ascending=True, inplace=True)

#plot_time(df)
if 'speedup' in args.chart:
    plot_speedup(df)
#
if 'time' in args.chart:
    plot_time_nth(df)
#
