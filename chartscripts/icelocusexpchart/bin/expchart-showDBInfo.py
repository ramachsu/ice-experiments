#!/usr/bin/env python

from os import path
import argparse, sys, logging

#print(f"Before loading dbutils from {__name__}")

from icelocusexpchart.common.charts.dbutils import showDBinfo

parser = argparse.ArgumentParser(description="Show info as much as "
                                        "possible info about the db.")
parser.add_argument('-i', type=str, required=True, help="Path to the db")

args=parser.parse_args()

if __name__ == '__main__':
    showDBinfo(args.i)
#
