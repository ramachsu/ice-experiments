#!/usr/bin/env python

import numpy as np, pandas as pd
import argparse, math, itertools, os
import matplotlib.pyplot as plt

from icelocusexpchart.common.charts.utils import (linestyles, markerstyles,
    patterns, colors)

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
parser.add_argument('--nlcol', type=int, required=False, help="number of columns"
        "in the legend.", default=1)
parser.add_argument("--chart", nargs='+', required=False,
        default=('gflops','speedup'),
        choices=('gflops','speedup'),
        help="Chart options: time and speedup")

args=parser.parse_args()

def calc_transp_flops(x):
    return (x*x*8)/1e9 # gigabytes
##

def plottime(dfinp) :
    df = dfinp[dfinp['matshapeM'] == dfinp['matshapeN']] 
    strgs = df.groupby('leg') #df['leg'].unique()
    linesty=itertools.cycle(linestyles)
    mksty  =itertools.cycle(markerstyles)

    for keyleg, stleg in strgs:
        print(stleg)
        ccs = stleg.groupby('comp')
        for keycc, stcc in ccs:
            x = stcc['matshapeM'].unique()
            #print("x",x)
            #y = st.groupby('len').median().time/1e3
            #y = st.groupby('len').min().time/1e3
            y = stcc.groupby(['matshapeM']).median().time*1.0e-3
            #ymin = st.groupby('len').min().time/1e3
            #ymax = st.groupby('len').max().time/1e3
            print(keyleg,keycc, y)
            label = keyleg+" "+keycc #st.leg

            plt.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')
        ##
    ##

    ticks=df['matshapeM'].unique()

    plt.grid(b=True, which='both', linestyle='-', alpha=0.2)
    #leg=str(size)
    #plt.errorbar(x, y, yerr=[ymin, ymax], label=leg, capsize=2)
    #plt.title("Eigenproblem on Stellar machine")
    plt.xticks(ticks)
    plt.tick_params(axis='x', labelsize=8, labelrotation=40.0)
    plt.xlabel("N")
    plt.ylabel("Time (sec)")
    plt.legend(loc='upper left')
    figname=args.o
    plt.savefig(figname, bbox_inches='tight')
    print(f"Saved chart in {figname} .")
#enddef

def plot_gtransf_perCC(dfinp, xlabel="N"):
    # get only the squared ones
    df = dfinp[dfinp['matshapeM'] == dfinp['matshapeN']] 

    datacomp = df.groupby('comp') #data['comp'].unique()
    nrows = 1
    ncols = len(datacomp)
    sbidx = 0
    fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(15,5),
            constrained_layout=True)

    for keycomp, grpcomp in datacomp:
        pltobj = axes[sbidx]
        linesty = itertools.cycle(linestyles)
        mksty = itertools.cycle(markerstyles)
        pltobj.grid(b=True, which='both',linestyle='-', alpha=0.2)

        strgs = grpcomp.groupby('leg') #df['leg'].unique()
        for keyleg, grpleg in strgs:
            print(f"==== {keycomp} {keyleg} ====")

            def percentile(n):
                def percentile_(x):
                    return np.percentile(x, n)
                percentile_.__name__ = 'percentile_%s' % n
                return percentile_
            #
            print(f"")

            pmin = 20
            pmax = 80

            print(f"{grpleg}")

            #variants = grpleg.groupby(['matshapeM','matshapeN','run']).aggregate([('med',np.median), ('min',min), ('max',max)])
            variants = grpleg.groupby(['matshapeM','matshapeN','run']).aggregate([('med',np.median),
                ('min', percentile(pmin)), ('max', percentile(pmax))])
            variants.columns = variants.columns.map('_'.join)
            variants = variants.reset_index()
            print(f"Variants 2: {type(variants)}\n{variants}")

            variants = variants.loc[variants.groupby(['matshapeM','matshapeN']).time_med.idxmin()] #.reset_index(drop=True)
            print(f"Variants 3: {type(variants)}\n{variants}")

            x = grpleg['matshapeM'].unique()
            xflop = calc_transp_flops(x) #x*x/1e9 
            ymed = variants.time_med
            ymax = variants.time_max
            ymin = variants.time_min

            #ymed = grpleg.groupby(['matshapeM']).time.median()
            #ymax = grpleg.groupby(['matshapeM']).time.quantile(.2)
            #ymin = grpleg.groupby(['matshapeM']).time.quantile(.8)
            #print(f"{keycomp} {keyleg} {ymed} {ymin} {ymax}"

            ymed_sec = ymed*1.0e-3
            ymax_sec = ymax*1.0e-3
            ymin_sec = ymin*1.0e-3
            y = xflop/ymed_sec
            yflopmax = (xflop/ymax_sec)-y
            yflopmin = y-(xflop/ymin_sec)
            #print(f"{keycomp} {keyleg} {y} {yflopmin} {yflopmax}")
            label = keyleg

            #pltobj.plot(x, y, label=label, marker=next(mksty),
            pltobj.errorbar(x, y, yerr=[yflopmin, yflopmax], label=label, capsize=2, marker=next(mksty),
                    linestyle=next(linesty), fillstyle='none')
        ##
        if sbidx == 1:
            lgd = pltobj.legend(loc='upper center', ncol=3)
        #
        if sbidx % nrows == (nrows - 1):
            pltobj.set_xlabel(xlabel)
        #
        if sbidx == 0:
            pltobj.set_ylabel("GBytes/sec")
        #
        ticks=grpcomp['matshapeM'].unique()
        pltobj.tick_params(axis='x', labelsize=8, labelrotation=40.0)
        pltobj.set_xticks(ticks)
        pltobj.set_title(keycomp)
        sbidx += 1
    ##

    name, ext = os.path.splitext(args.o)
    figname= name+"_gflops"+ext
    #fig.tight_layout()
    plt.savefig(figname)#,
            #bbox_extra_artists=(lgd,),
            #bbox_inches='tight')
    print(f"Saved chart in {figname} .")
###

def plot_gtransf_perth_perCC(dfinp, xlabel="N"):
    # get only the squared ones
    df = dfinp[dfinp['matshapeM'] == dfinp['matshapeN']] 

    nth = df['numthreads'].unique()
    ncc = df['comp'].unique()
    nrows = len(nth)
    ncols = len(ncc)
    sbidx = 0
    #fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(15,5))
    fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(12,16),
            constrained_layout=True)#sharex=True,
    lgd = None
    sbidx = 0
    sbidy = 0
    sbid = 0
    print(f"nrows: {nrows} ncols: {ncols}")

    linesty = itertools.cycle(linestyles)
    mksty = itertools.cycle(markerstyles)

    dlegsty = {e: (l,m,c) for e,l,m,c in zip(df['leg'].unique(),linesty, mksty,
        colors) }
    #print(dlegsty)

    datath = df.groupby('numthreads') #data['comp'].unique()
    for keyth, grpth in datath:
        datacomp = grpth.groupby('comp') #data['comp'].unique()
        for keycomp, grpcomp in datacomp:
            #pltobj = axes[sbidx]
            if ncols > 1 and nrows > 1:
                pltobj =  axes[sbidx][sbidy]
            elif ncols > 1:
                # just single line
                pltobj = axes[sbidy]
            else:
                #ncols == 1 and nrwos == 1
                pltobj = axes
            #

            pltobj.grid(b=True, which='both',linestyle='-', alpha=0.2)

            strgs = grpcomp.groupby('leg') #df['leg'].unique()
            for keyleg, grpleg in strgs:
                print(f"==== {keyth} {keycomp} {keyleg} ====")

                def percentile(n):
                    def percentile_(x):
                        return np.percentile(x, n)
                    percentile_.__name__ = 'percentile_%s' % n
                    return percentile_
                #

                pmin = 20
                pmax = 80

                print(f"{grpleg}")

                #variants = grpleg.groupby(['matshapeM','matshapeN','run']).aggregate([('med',np.median), ('min',min), ('max',max)])
                variants = grpleg.groupby(['numthreads','matshapeM','matshapeN','run']).aggregate([('med',np.median),
                    ('min', percentile(pmin)), ('max', percentile(pmax))])
                variants.columns = variants.columns.map('_'.join)
                variants = variants.reset_index()
                print(f"Ops Variants 2: {type(variants)}\n{variants}")

                variants = variants.loc[variants.groupby(['numthreads','matshapeM','matshapeN']).time_med.idxmin()] #.reset_index(drop=True)
                print(f"Ops Variants 3: {type(variants)}\n{variants}")

                x = grpleg['matshapeM'].unique()
                xflop = calc_transp_flops(x) #x*x/1e9 
                ymed = variants.time_med
                ymax = variants.time_max
                ymin = variants.time_min

                #ymed = grpleg.groupby(['matshapeM']).time.median()
                #ymax = grpleg.groupby(['matshapeM']).time.quantile(.2)
                #ymin = grpleg.groupby(['matshapeM']).time.quantile(.8)
                #print(f"{keycomp} {keyleg} {ymed} {ymin} {ymax}"

                ymed_sec = ymed*1.0e-3
                ymax_sec = ymax*1.0e-3
                ymin_sec = ymin*1.0e-3
                y = xflop/ymed_sec
                yflopmax = (xflop/ymax_sec)-y
                yflopmin = y-(xflop/ymin_sec)
                #print(f"{keycomp} {keyleg} {y} {yflopmin} {yflopmax}")
                label = keyleg

                #pltobj.plot(x, y, label=label, marker=next(mksty),
                #pltobj.errorbar(x, y, yerr=[yflopmin, yflopmax], label=label, capsize=2, marker=next(mksty),
                #        linestyle=next(linesty), fillstyle='none')
                pltobj.errorbar(x, y, yerr=[yflopmin, yflopmax], label=label,
                        capsize=2, marker=dlegsty[label][1], 
                        color=dlegsty[label][2],
                        linestyle=dlegsty[label][0], fillstyle='none')
            ##
            if (sbidy == 1 and sbidx == 0) or (sbidx == 0 and ncols == 1):
            #if (sbidx == 0 and ncols == 1) or (sbidy == 1 and sbidx == 0):
            #if (sbidy == 1 ) or (sbidx == 0 and ncols == 1):
                lgd = pltobj.legend(loc='upper center', ncol=args.nlcol)
            ##
            if sbidx % nrows == (nrows - 1):
                xlabel = 'N'
                pltobj.set_xlabel(xlabel)
            #
            if sbidy == 0:
                pltobj.set_ylabel("GBytes/sec")
            #

            pltobj.spines['top'].set_visible(False)
            pltobj.spines['right'].set_visible(False)
            #pltobj.spines['left'].set_visible(False)
            #pltobj.spines['bottom'].set_color('#DDDDDD')

            def Log2(x):
                return (math.log10(x)/math.log10(2))
            ###
            #ticks=[x for x in grpcomp['matshapeM'].unique() if
            #        math.ceil(Log2(x)) == math.floor(Log2(x))]
            shapesM = grpcomp['matshapeM'].unique()
            minshM  = min(shapesM)
            ticks= [x for x in shapesM if x % minshM == 0 ]
            pltobj.tick_params(axis='x', labelsize=8) #, labelrotation=40.0)
            pltobj.set_xticks(ticks)
            if keyth == 1:
                pltobj.set_title(str(keyth)+" thread - "+keycomp)
            else:
                pltobj.set_title(str(keyth)+" threads - "+keycomp)
            #
            sbid += 1
            sbidy = sbid % ncols
            sbidx = sbid // ncols
        ##
    ##

    figname=args.o
    name, ext = os.path.splitext(args.o)
    figname= name+"_gflops"+ext
#    fig.suptitle(f"Mattransp Shape {shape}") 
    #fig.tight_layout()
    plt.savefig(figname) #,
            #bbox_extra_artists=(lgd,),
            #bbox_inches='tight')
    print(f"Saved chart in {figname} .")
###

def autolabel(rects, pltobj, lim=None):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        #if lim is None or height > lim:
        vpos = height if lim is None or height < lim else 0.9*lim
        pltobj.text(rect.get_x() + rect.get_width()/2., vpos,
            #f'{int(height)}',
            f'{height:.1f}',
            ha='center', va='bottom', fontsize='x-small')
        #
    ##
###

def plot_speedup(dfinp, shape=8192, baseName='Baseline'):
    df = dfinp[dfinp['matshapeM'] == shape] #.sort_values(['numthreads'], ascending=True)
    df = df[df['matshapeM'] == df['matshapeN']]
    datacc = df.groupby('comp')

    nrows = len(datacc) #1
    ncols = 1 #len(datacc)
    fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(ncols*9,nrows*5), constrained_layout=True)
    #fig.suptitle(f"Matrix Transpose over baseline (N={shape})") 
    #fig, axes = plt.subplots(nrows, ncols, sharey=True,  constrained_layout=True)
    lgd = None
    sbidx = 0
    sbidy = 0
    sbid = 0

    for keycc, grpcc in datacc:
        if ncols > 1 and nrows > 1:
            pltobj =  axes[sbidx][sbidy]
        elif ncols > 1:
            # just single line
            pltobj = axes[sbidy]
        elif nrows > 1:
            pltobj = axes[sbidx]
        else:
            #ncols == 1 and nrwos == 1
            pltobj = axes
        #

        linesty = itertools.cycle(linestyles)
        mksty = itertools.cycle(markerstyles)

        variants = grpcc[grpcc['leg'] == baseName].groupby(['numthreads', 'matshapeM', 'matshapeN', 'run']).aggregate(
                    [('med',np.median), ('min', min), ('max', max)])
        print(f"%%% Base Variants: {type(variants)} {variants}")

        variants.columns = variants.columns.map('_'.join)
        variants = variants.reset_index()

        nthreads = len(grpcc['numthreads'].unique())
        baseline = variants[variants['numthreads'] == 1].time_med.repeat(nthreads).reset_index(drop=True)
        #baseline = variants[variants['numthreads'] == 1].time_med.repeat(variants.shape[0]).reset_index(drop=True)
        #baseline = variants.time_med.repeat(variants.shape[0]).reset_index(drop=True)
        print(f"Base: {type(baseline)} {baseline}")


        dataleg = grpcc.groupby('leg')
        nbars = len(grpcc['leg'].unique())
        barwidth = 1.0/nbars
        curbarpos = -0.5+barwidth
        pat =itertools.cycle(patterns)
        col =itertools.cycle(colors)
        for keyleg, gprleg in dataleg:
            if keyleg != baseName:
                print(f"==== {keycc} {keyleg} ====")

                # get the fastest baseline variant for this strategy
                variants = gprleg.groupby(['numthreads', 'matshapeM', 'matshapeN', 'run']).aggregate(
                        [('med',np.median), ('min', min), ('max', max)])

                variants.columns = variants.columns.map('_'.join)
                variants = variants.reset_index()
                print(f"Variants 2: {type(variants)}\n{variants}")

                variants = variants.loc[variants.groupby(['matshapeM', 'matshapeN','numthreads']).time_med.idxmin()]
                #print(f"Variants 3: {type(variants)}\n{variants}")

                x = gprleg['numthreads'].unique()
                y = baseline.div(variants.time_med.reset_index(drop=True))
                #print(f" var: {type(variants.time_med)} {variants.time_med}  y: {y}")
                label = keyleg
                #pltobj.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')
                print(f"X Values: {x+curbarpos}")
                rects = pltobj.bar(x+curbarpos, y, label=label,
                        width=barwidth-0.05,
                        hatch=next(pat),
                        color='white',
                        edgecolor=next(col))
                yupperb = 0.5
                autolabel(rects, pltobj) #, yupperb)
                curbarpos += barwidth 
            #
        ##
        #pltobj.grid(b=True, which='y', linestyle='-', alpha=0.2)
        pltobj.yaxis.grid(True, color='#EEEEEE', alpha=0.2)
        pltobj.xaxis.grid(False)
        if (sbidy == 1 and sbidx == 0) or (sbidx == 0 and ncols == 1):
            lgdloc = 'upper left'  #'lower center'
            lgd = pltobj.legend(loc=lgdloc, ncol=args.nlcol, framealpha=0.1)
        ##
        if sbidx % nrows == (nrows - 1):
            xlabel = 'Number of Threads'
            pltobj.set_xlabel(xlabel)
        #
        if sbidy == 0:
            pltobj.set_ylabel('Speedup')
        #
        pltobj.spines['top'].set_visible(False)
        pltobj.spines['right'].set_visible(False)
        pltobj.spines['left'].set_visible(False)
        #pltobj.spines['bottom'].set_color('#DDDDDD')
        pltobj.spines['bottom'].set_color('black')

        threadslbl = grpcc['numthreads'].unique()
        #ticks= threadslbl+((nbars/2)*barwidth)
        #pltobj.tick_params(axis='x', labelsize=8) #, labelrotation=40.0)
        pltobj.set_xticks(threadslbl)
        pltobj.tick_params(left=False)
        pltobj.set_title(keycc)
        sbid += 1
        sbidy = sbid % ncols
        sbidx = sbid // ncols

        # Generate optimal speedup
        if False:
            x = df['numthreads'].unique()
            y = [*range(1, len(x)+1)]
            #print(f"linear: {x} {y}")
            pltobj.plot(x, y, linestyle='-', color='k', alpha=1.0, linewidth=0.5)
        #
    ##

    name, ext = os.path.splitext(args.o)
    figname= name+"_speedup"+ext
    #fig.tight_layout()
    #fig.subplots_adjust(top=0.8)
    fig.savefig(figname)
                #bbox_extra_artists=(lgd,),
                #bbox_inches='tight')
    print(f"Saved chart in {figname} .")
###

df = pd.read_csv(args.i, comment="#")
print(df)
print(f"Reading file {args.i} ...")
#print(f"Plot selected {args.p}!")

df.sort_values(['numthreads','leg','matshapeM','matshapeN'], ascending=True, inplace=True)

#plottime(df)

if 'gflops' in args.chart:
    #plot_gtransf_perCC(df)
    plot_gtransf_perth_perCC(df)
#
if 'speedup' in args.chart:
    plot_speedup(df)
#
