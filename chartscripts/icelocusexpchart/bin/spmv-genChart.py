#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse, math, os
import matplotlib.pyplot as plt
import matplotlib as mpl
import itertools, math

from icelocusexpchart.common.charts.utils import linestyles, markerstyles, autolabel #,patterns, colors

mpl.rcParams['hatch.linewidth'] = 0.1
colors = ['red', 'gold', 'limegreen', 'deepskyblue', 'purple',
        'darkblue', 'dodgerblue', 'tab:olive', 'tab:cyan', 'tab:pink']
patterns = ["."*5, "x"*5, "-"*3, '\\\\' , '///' , "++" , "---", "xxx", "...", "o", "*", "O" ]

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
parser.add_argument("--chart", nargs='+', required=False,
        default=('time',),
        choices=('time','speedup','quad','bars'),
        help="Select chart type")

args=parser.parse_args()

def plot_time_per_mat_quadrant(dfinp, speedup=True):
    ''' This is for evaluating sparse matrix per quadrant '''

    nmats = len(dfinp['mat'].unique())
    nblks = dfinp['nblocks'].unique()[0]
    dfinp["leg"] = dfinp["fmt"] + "-" + dfinp["fmtblk"].map(str)

    datamat = dfinp.groupby('mat')
    for keymat, grpmat in datamat:
        nrows = nblks
        ncols = nblks

        matname =  os.path.splitext(os.path.basename(keymat))[0]
        print(type(nrows), nrows,type(ncols), ncols)
        fig, axes = plt.subplots(nrows, ncols, sharey='row', figsize=(5*ncols,5*nrows), constrained_layout=True)
        fig.suptitle(f"SpMV per quandrant")

        sbidx = 0
        sbidy = 0
        sbid = 0

        datablks = grpmat.groupby(['blockx','blocky'])
        for xblk in range(nblks):
            for yblk in range(nblks):
                for keyblk, grpblk  in datablks:
                    if xblk == keyblk[0] and yblk == keyblk[1]:
                        pltobj = axes[xblk][yblk]
                        #pltobj = axes[sbidy]

                        nbars = len(grpblk['leg'].unique())

                        #print("Grpbllk",grpblk)
                        ind = np.arange(nbars)
                        variants = grpblk.groupby(['mat','leg','fmt','fmtblk','nblocks','blockx','blocky','indx','indendx','indy','indendy','nz', 'iterations']).aggregate(
                                        [('med',np.median), ('min', min), ('max', max)])
                        print(f"Th: {keyblk} Variants: {type(variants)}\n{variants}")

                        variants.columns = variants.columns.map('_'.join)
                        variants = variants.reset_index()
                        print(f"Th: {keyblk} Variants 2: {type(variants)}\n{variants}")

                        variants = variants.loc[variants.groupby(['mat','leg','fmt','fmtblk','nblocks','blockx','blocky','indx','indendx','indy','indendy','nz', 'iterations']).time_med.idxmin()]
                        print(f"Th: {keyblk} Variants 3: {type(variants)}\n{variants}")

                        if speedup:
                            nlegs = len(variants['leg'].unique())
                            refcsr = variants[variants['leg'] == 'CSR-0'].time_med.repeat(nlegs).reset_index(drop=True)
                            y = refcsr.div(variants.time_med.reset_index(drop=True))
                            print(f"Th: {keyblk} Variants 4: {type(y)}\n{y}")
                            xlbls = variants.leg.unique()
                        else:
                            svar = variants.sort_values(by=['time_med'], ascending=False)
                            print(f"Th: {keyblk} Variants 4: {type(svar)}\n{svar}")
                            y = svar.time_med #variants.time_med
                            xlbls = svar.leg.unique()
                        #

                        barwidth = 0.7 #1.0/nbars
                        rects = pltobj.bar(ind, y, width=barwidth)
                        autolabel(rects, pltobj) #, yupperb)

                        pltobj.spines['top'].set_visible(False)
                        pltobj.spines['right'].set_visible(False)
                        pltobj.spines['left'].set_visible(False)
                        pltobj.spines['bottom'].set_color('#DDDDDD')
                        pltobj.tick_params(bottom=False, left=False)
                        pltobj.set_xticks(ind)
                        pltobj.set_axisbelow(True)
                        pltobj.yaxis.grid(True, color='#EEEEEE')
                        pltobj.xaxis.grid(False)
                        print("xlbls", xlbls)
                        pltobj.set_xticklabels(xlbls)
                        pltobj.set_title(matname+" - "+str(keyblk))
                    #
                #

                #sbid += 1
                #sbidy = sbid % ncols
                #sbidx = sbid // ncols
            #
        #

        name, ext = os.path.splitext(args.o)
        if speedup:
            figname= name+"_speedup_"+matname+ext
        else:
            figname= name+"_time_"+matname+ext
        #
        fig.savefig(figname)
        print(f"Saved chart in {figname} .")
    #
###

def plot_time_per_mat(dfinp, speedup=True, reflbl='1lCSR'):
    print(dfinp.dtypes)
    nmats = len(dfinp['mat'].unique())
    nths = len(dfinp['numthreads'].unique())
    if nths > 1:
        ncols = nths
        nrows = nmats
    else:
        ncols = min(nmats, 1)
        nrows = math.ceil( nmats / ncols)
    #
    print(f"rows {nrows} x {ncols} cols")
    #fig, axes = plt.subplots(nrows, ncols, sharey='row', figsize=(5*ncols,5*nrows), constrained_layout=True)
    fig, axes = plt.subplots(nrows, ncols, figsize=(15*ncols,5*nrows), constrained_layout=True)
    fig.suptitle(f"SpMV")

    lgd = None
    sbidx = 0
    sbidy = 0
    sbid = 0

    datamat = dfinp.groupby('mat')
    for keymat, grpmat in datamat:
        datath = grpmat.groupby('numthreads')
        for keyth, grpth in datath:
            print(f"{keymat} {keyth}")
            if ncols > 1 and nrows > 1:
                pltobj =  axes[sbidx][sbidy]
            elif ncols > 1:
                # just single line
                pltobj = axes[sbidy]
            elif nrows > 1:
                pltobj = axes[sbidx]
            else:
                #ncols == 1 and nrwos == 1
                pltobj = axes
            #

            linesty = itertools.cycle(linestyles)
            mksty = itertools.cycle(markerstyles)

            nbars = len(grpth['leg'].unique())
            ind = np.arange(nbars)
            variants = grpth.groupby(['leg', 'numthreads','mat','run']).aggregate(
                            [('med',np.median), ('min', min), ('max', max)])
            print(f"Th: {keyth} Variants: {type(variants)}\n{variants}")

            variants.columns = variants.columns.map('_'.join)
            variants = variants.reset_index()
            print(f"Th: {keyth} Variants 2: {type(variants)}\n{variants}")

            variants = variants.loc[variants.groupby(['leg','mat','numthreads']).time_med.idxmin()]
            print(f"Th: {keyth} Variants 3: {type(variants)}\n{variants}")

            if speedup:
                ref = variants[variants['leg'] == reflbl]
                if len(ref):
                    nbest = variants.nsmallest(20, 'time_med')
                    nlegs = len(nbest['leg'].unique())
                    refcsr = variants[variants['leg'] == reflbl].time_med.repeat(nlegs).reset_index(drop=True)
                    y = refcsr.div(nbest.time_med.reset_index(drop=True))
                    print(f"Th: {keyth} Variants 4: {type(y)}\n{y}")
                    xlbls = nbest.leg.unique()
                    ind = np.arange(len(xlbls))
                else:
                    print(f"Warning!!! {keymat} Th: {keyth} does not have ref!!! \n")
                    continue
                #
            else:
                svar = variants.sort_values(by=['time_med'], ascending=False)
                print(f"Th: {keyth} Variants 4: {type(svar)}\n{svar}")
                y = svar.time_med #variants.time_med
                xlbls = svar.leg.unique()
            #

            barwidth = 0.7 #1.0/nbars
            rects = pltobj.bar(ind, y, width=barwidth)
            autolabel(rects, pltobj) #, yupperb)

    #        dataleg = grpmat.groupby('leg')
    #        #curbarpos = -0.5+barwidth
    #        x = 1
    #        xlbls = []
    #
    #        for keyleg, grpleg in dataleg:
    #            variants = grpleg.groupby(['numthreads', 'mat', 'run']).aggregate(
    #                        [('med',np.median), ('min', min), ('max', max)])
    #
    #            variants.columns = variants.columns.map('_'.join)
    #            variants = variants.reset_index()
    #            print(f"Variants 2: {type(variants)}\n{variants}")
    #
    #            variants = variants.loc[variants.groupby(['mat','numthreads']).time_med.idxmin()]
    #
    #            #x = grpleg['leg'].unique()
    #            xlbls.append(keyleg)
    #            label = keyleg
    #            y = variants.time_med
    #            pltobj.bar(x, y, label=label, width=barwidth)
    #            x += 1
    #        ##

            #pltobj.grid(b=True, which='both', linestyle='-', alpha=0.2)
            #if (sbidy == 1 and sbidx == 0) or (sbidx == 0 and ncols == 2):
            #lgd = pltobj.legend(loc='upper right', ncol=3, framealpha=0.9)
            ##
            if sbidx % nrows == (nrows - 1):
                xlabel = 'Storage Format'
                pltobj.set_xlabel(xlabel)
            #
            if sbidy == 0:
                pltobj.set_ylabel('Execution Time (milisec)')
            #
            #threadslbl = grpcc['numthreads'].unique()
            #pltobj.tick_params(axis='x', labelsize=8) #, labelrotation=40.0)

            pltobj.spines['top'].set_visible(False)
            pltobj.spines['right'].set_visible(False)
            pltobj.spines['left'].set_visible(False)
            pltobj.spines['bottom'].set_color('#DDDDDD')
            pltobj.tick_params(bottom=False, left=False)
            pltobj.set_xticks(ind)
            pltobj.set_axisbelow(True)
            pltobj.yaxis.grid(True, color='#EEEEEE')
            pltobj.xaxis.grid(False)
            print("xlbls", xlbls)
            pltobj.set_xticklabels(xlbls, rotation=45)
            pltobj.set_title(keymat+" - "+str(keyth)+"ths")
            sbid += 1
            sbidy = sbid % ncols
            sbidx = sbid // ncols
        ##
    ##

    # Set off the charts not used
    while sbid < (ncols*nrows):
        if ncols > 1 and nrows > 1:
            pltobj =  axes[sbidx][sbidy]
        elif ncols > 1:
            # just single line
            pltobj = axes[sbidy]
        elif nrows > 1:
            pltobj = axes[sbidx]
        else:
            #ncols == 1 and nrwos == 1
            pltobj = axes
        #
        pltobj.axis('off')
        sbid += 1
        sbidy = sbid % ncols
        sbidx = sbid // ncols
    #
    name, ext = os.path.splitext(args.o)
    figname= name+"_time"+ext
    fig.savefig(figname)
    print(f"Saved chart in {figname} .")
###

def plot_bars(dfinp):
    """Plot grouped bars by legend"""

    nrows,ncols = 1,1
    fig, axes = plt.subplots(nrows, ncols, sharey=True, figsize=(ncols*7,nrows*3),
            constrained_layout=True)

    pltobj = axes
    nbars = len(dfinp['leg'].unique())
    dataleg = dfinp.groupby('leg')
    onemat = 2
    barwidth = (onemat*0.8)/nbars
    #curbarpos = (-(onemat/2.0))#+barwidth
    curbarpos = (-(nbars/2.0)*barwidth)+(barwidth/2.0)
    print(f"nbars: {nbars} barwidth: {barwidth} curbarpos: {curbarpos}")
    pat =itertools.cycle(patterns)
    col =itertools.cycle(colors)
    for keyleg, gprleg in dataleg:
        print(f"==== {keyleg}  ====")
        #x = list(range(1,len(gprleg['matrix'].unique())+1))
        x = np.array(range(1,onemat*(len(gprleg['matrix'].unique()))+1, onemat))
        #x = np.ndarray(len(gprleg['matrix'].unique()))
        #y = gprleg.groupby(['matrix','run'])
        y = gprleg['time']
        label = keyleg
        print(f"{x} | {y} | {label}")
        print(f"X Values: {x+curbarpos}")
        pltobj.grid(b=True, which='major', axis='y', linestyle='-', alpha=0.2)
        rects = pltobj.bar(x+curbarpos, y, label=label, width=barwidth-0.05,
                        hatch=next(pat), color='white', linewidth=0.4, edgecolor=next(col))
        yupperb = 0.5
        autolabel(rects, pltobj, prec=2) #, yupperb)
        curbarpos += barwidth 
    ##
    lgdloc = 'upper center' #'upper center'
    lgd = pltobj.legend(loc=lgdloc, ncol=7, framealpha=0.1, prop={'size': 8})
    #pltobj.tick_params(axis='x', labelsize=8) #, labelrotation=40.0)
    pltobj.spines['top'].set_visible(False)
    pltobj.spines['right'].set_visible(False)
    pltobj.spines['left'].set_visible(False)
    #pltobj.spines['bottom'].set_color('#DDDDDD')
    pltobj.spines['bottom'].set_color('black')

    ticks=dfinp['matrix'].unique()
    pltobj.set_xticks(range(1,onemat*(len(ticks))+1,onemat))
    pltobj.set_xticklabels(ticks)
    pltobj.set_yticks([1,2])
    pltobj.set_ylim([0,3])
    pltobj.tick_params(left=False)
    pltobj.set_ylabel('Speedup over CSR')
    name, ext = os.path.splitext(args.o)
    #fig.suptitle(f"Matmul Shape {shape}") 
    figname= name+"_speedup"+ext
    #fig.tight_layout()
    fig.savefig(figname)
    print(f"Saved chart in {os.path.abspath(figname)} .")

###

df = pd.read_csv(args.i, comment="#")
#print(df)
print(f"Reading file {args.i} ...")
#print(f"Plot selected {args.p}!")
#print(df.info)

if 'time' in args.chart:
    plot_time_per_mat(df)
elif 'bars' in args.chart:
    plot_bars(df)
elif 'quad' in args.chart:
    df.sort_values(['mat','fmt','fmtblk','nblocks','blockx','blocky'], ascending=True, inplace=True)
    print(df.dtypes)
    #print(df)
    plot_time_per_mat_quadrant(df)
#

