import logging

#
# this NullHandler makes sure that the log calls on the library do not turn in error, and
# let the application using define the handler.
#
logging.getLogger(__name__).addHandler(logging.NullHandler())

#print(f"Executed {__name__}")
