Locus/ICE Experiemtns and Chart Library
================


These are auxiliary library to run and generates charts automatically.

Installation
-------

1. Install the virtual environment tool: `pip install virtualenv`
2. Create a virtual environment: `virtualenv <path>/<to>/<venv> --python=python3`
3. Activate the virtual environment: `source <path/<to>>/<venv>/bin/activate`
4. Go to ice-locus-expchart path
5. Install: `python setup.py install|develop`
6. Check installation: `mattransp-runexps.py --help`


Methodology
-------

1. Run scripts, the data will be saved on the locus.db/<mach>.db
2. Get data from DB to csv.
3. Generate chart from csv.
