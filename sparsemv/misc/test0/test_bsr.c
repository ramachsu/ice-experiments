/* 
*   Matrix Market I/O example program
*
*   Read a real (non-complex) sparse matrix from a Matrix Market (v. 2.0) file.
*   and copies it to stdout.  This porgram does nothing useful, but
*   illustrates common usage of the Matrix Matrix I/O routines.
*   (See http://math.nist.gov/MatrixMarket for details.)
*
*   Usage:  a.out [filename] > output
*
*       
*   NOTES:
*
*   1) Matrix Market files are always 1-based, i.e. the index of the first
*      element of a matrix is (1,1), not (0,0) as in C.  ADJUST THESE
*      OFFSETS ACCORDINGLY offsets accordingly when reading and writing 
*      to files.
*
*   2) ANSI C requires one to use the "l" format modifier when reading
*      double precision floating point numbers in scanf() and
*      its variants.  For example, use "%lf", "%lg", or "%le"
*      when reading doubles, otherwise errors will occur.
*/

#include <io.h>
#include <string.h>
#include <mkl_types.h>
#include <mkl_spblas.h>

int main(int argc, char *argv[])
{
    FILE *f;
    int M, N, nz, origM, origN;
    int i, *I, *J;
    double *val;
    int symmetric = 0;
    double alpha = 1.0, beta = 0.0;

    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s [martix-market-filename]\n", argv[0]);
        exit(1);
    }
    else
    {
        if ((f = fopen(argv[1], "r")) == NULL) 
            exit(1);
    }

    readHeader(f, &M, &N, &nz, &symmetric);

    origM = M;
    origN = N;
    int patchmatshape = 1;
    if (patchmatshape) 
    {
        int mul = 32;
        int patch = (mul-(M % mul));
        M += patch;
        N += patch;
        printf("Patched shapes to accept BCSR blocks... New values M=%d N=%d\n",M, N);
    }

    /* reseve memory for matrices */
    I = (int *) malloc(nz * sizeof(int));
    J = (int *) malloc(nz * sizeof(int));
    val = (double *) malloc(nz * sizeof(double));


    printf("Reading matrix in MTX format...\n");
    /* NOTE: when reading in doubles, ANSI C requires the use of the "l"  */
    /*   specifier as in "%lg", "%lf", "%le", otherwise errors will occur */
    /*  (ANSI C X3.159-1989, Sec. 4.9.6.2, p. 136 lines 13-15)            */
    for (i=0; i<nz; i++)
    {
        fscanf(f, "%d %d %lg\n", &I[i], &J[i], &val[i]);
        I[i]--;  /* adjust from 1-based to 0-based */
        J[i]--;
#ifdef DEBUG
        fprintf(stderr, "(%d, %d) %7.5lf ", J[i], I[i], val[i]);
#endif
    }
#ifdef DEBUG
    fprintf(stderr,"\n====\n");
#endif

    if (f !=stdin) fclose(f);

    printf("Converting to MKL COO M= %d N= %d nz= %d symmetric= %d\n", M, N, nz, symmetric);
    /************************/
    /* Calculation */
    /************************/

    double * x = (double *) malloc(N * sizeof(double));
    double * y = (double *) malloc(M * sizeof(double));
    for (i=0; i < N; ++i) {
        x[i] = (double) i;

#ifdef DEBUG
        fprintf(stderr, "%7.5lf ", x[i]);
#endif
    }
#ifdef DEBUG
    fprintf(stderr,"\n====\n");
#endif
    memset(y, 0, M * sizeof(double));

    sparse_matrix_t Acoo;
    sparse_index_base_t indexing = SPARSE_INDEX_BASE_ZERO;
    sparse_status_t stat = mkl_sparse_d_create_coo(&Acoo, indexing, M, N, nz, I, J, val);
    if (stat != SPARSE_STATUS_SUCCESS) {
        fprintf(stderr, "MKL COO creation failed!\n");
    }

    sparse_matrix_t Acsr;
    sparse_operation_t op = SPARSE_OPERATION_NON_TRANSPOSE;

    printf("Converting from COO to CSR ...\n");
    stat = mkl_sparse_convert_csr( Acoo, op, &Acsr );
    if (stat != SPARSE_STATUS_SUCCESS) {
        fprintf(stderr, "MKL COO->BSR convertion failed! %d\n",stat);
        exit(1);
    }

    mkl_sparse_destroy(Acoo);

    int block_size = 32;
    sparse_layout_t blay = SPARSE_LAYOUT_ROW_MAJOR;
    sparse_matrix_t Absr;
    printf("Converting from CSR to BSR block= %d...\n", block_size);
    stat = mkl_sparse_convert_bsr( Acsr, block_size,  blay, op, &Absr );
    if (stat != SPARSE_STATUS_SUCCESS) {
        fprintf(stderr, "MKL CSR->BSR convertion failed! %d\n",stat);
        exit(1);
    }

    printf("SpMV...\n");
    //sparse_operation_t op = SPARSE_OPERATION_NON_TRANSPOSE;
    //sparse_matrix_type_t mattype = SPARSE_MATRIX_TYPE_GENERAL;
    struct matrix_descr descr;
    if(symmetric) { 
        descr.type = SPARSE_MATRIX_TYPE_SYMMETRIC;
        descr.mode = SPARSE_FILL_MODE_LOWER;
        descr.diag = SPARSE_DIAG_NON_UNIT;
    } else {
        descr.type = SPARSE_MATRIX_TYPE_GENERAL;
    }
    stat = mkl_sparse_d_mv(op, alpha, Acsr, descr, x, beta, y);
    if (stat != SPARSE_STATUS_SUCCESS) {
        fprintf(stderr, "MKL SpMV failed!\n");
    }

    mkl_sparse_destroy(Acsr);

    fprintf(stdout, "Sparse %s M= %d N= %d nz= %d\n", argv[1], origM, origN, nz);
    if(fopen(".test", "r")) {
        for(i=0; i < origN; ++i) {
            fprintf(stderr, "%7.5lf ", y[i]);
        }
    }

    //printf("Done\n");

    free(I);
    free(J);
    free(val);
    free(x);
    free(y);
//    sparse_matrix_t *A;
//    int rows = M, cols = N; 
//
//    sparse_index_base_t indexing = SPARSE_INDEX_BASE_ZERO;
//    stat = mkl_sparse_d_create_csr ( A , indexing , rows , cols , rows_start , rows_end , col_indx , val );
//    if(stat != SPARSE_STATUS_SUCCESS) {
//        fprintf(stderr, "Error on mkl_sparse_d_create_csr!\n")
//        exit(1);
//    }
//
//    transa = 'N';
//    matdescra[0] = '';
//    matdescra[1] = '';
//    matdescra[2] = '';
//    mkl_dbsrmv();
//
//    free(rows_st);
//    free(rows_end);
//    free(col_indx);
//    free(val);
//
//    return 0;
}

