//#include <lapacke.h>
#include "util.h"

using namespace std;

int main(int argc, char** argv)
{
/*
 *  Calculate eigenproblem n x n symmetric matrix
 */
    int ninp = 0;
    int niterations = 5;
    float checkrate = 1.0;
    string inpname = "N/A";

    if (argc >= 3) {
        ninp = std::stoi(argv[1]);
        inpname = argv[2];
        if (argc >= 4) {
            checkrate = std::stof(argv[3]);
        }
    } else {
        std::cerr << "Usage: ./<bin> <dim> <file path> [<checkrate of results>]" << endl;
        return 1;
    }
    int neigeneval = (int) ninp*checkrate;
    std::cout << "Symmetric Eigenproblem n: " << ninp << " inp: " << inpname << 
        " checking rate: " << checkrate << " (" << neigeneval << " eigenvalues and vectors)" << std::endl;

    lapack_int n=ninp, lda=n, ldc=n, ldz=n, lwork;
    TYPE * Aread = new TYPE[n*n];
    TYPE * A = new TYPE[n*n];
    TYPE * Z = new TYPE[n*n];
    TYPE * D = new TYPE[n];
    TYPE * E = new TYPE[n-1];
    TYPE * tau = new TYPE[n]; //new TYPE[n-1];
    TYPE * work = new TYPE[1];
    char uplo = 'U';
    char trans = 'N';
    char side = 'L';

    double t_start, t_end;
    double * t_it = new double[niterations];
    int incorrect = false;
    TYPE* LHS= new TYPE[n*n];
    TYPE* RHS= new TYPE[n*n]; 
    TYPE* RHStransp = new TYPE[n*n]; 

    if(int ret = readfile(inpname, Aread, ninp)) return ret;

#ifdef DEBUG
    cout << "Aread:" << endl;
    writeresult("out", Aread, n, n);
#endif
#ifdef DEBUG
    cout << "Starting lapack..." << endl;
#endif
    int rundctest = 0;
    if (rundctest) {
        cout << "Running DC test two QRTD!" << endl;
    } else {
        cout << "Running DC LAPACKE_dstedc_work!" << endl;
    }
    cout << "Eigproblem= "<< n << " Iterations= " << niterations << " Time(milisec)=";
    for (int it = 0; it < niterations; it++) {

        int index = 0;
        // Pass the data in column major
        for(int j=0; j < n; j++) {
            for(int i=0; i < n; i++ ) {
                //cout << "A[" << index << "] = " << Aread[i*n+j]  << endl;
                A[index++] = Aread[i*n+j];
            }
        }

        //
        // Calculate the T = QAQ', tridiagonal matrix
        // 
        t_start = rtclock();
        lwork = -1;
        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;

        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd II failed!" << endl;
            return ret;
        }

        delete [] work;
        ///////////////

        //
        // Call QRTD twice on the subproblems
        //
        //

        if(rundctest) {
            int  cutpnt = n/2;
            TYPE rho = E[cutpnt-1];
            //cout << "RHO =" << rho << endl;
            //TYPE * q1 = new TYPE[(n/2)*(n/2)]
            //TYPE * q2 = new TYPE[(n/2)*(n/2)]
            //TYPE * Dtmp = new TYPE[]
            //int wSize = (n/2)*(n/2)+ 4*(n/2);
            //TYPE * work1 = new TYPE[wSize];
            //TYPE * work2 = new TYPE[wSize];


            /*lwork=2*n-2;
            TYPE * work2 = new TYPE[std::max(1, lwork)];
            if(lapack_int ret = LAPACKE_dsteqr_work(LAPACK_COL_MAJOR, compz, n, D, E, Z, ldz, work2)) {
                cerr << "[Error] dsteqr failed!" << endl;
                return ret;
            }
            delete [] work2;
            */

            double one = 1.0;
            double zero = 0.0;
            char uplo = 'F';
            laset_wrap(&uplo, &n, &n, &zero, &one, Z, &ldz);
            //cerr << "Z after laset: " << endl ;
            //writeresult("out", Z, n, n);

            //TYPE * eigval = new TYPE[n];
            TYPE * tmp = new TYPE[n*n];
            TYPE * q1 = Z; //tmp;
            TYPE * q2 = &Z[(n/2)*ldz+(n/2)];

            // Scale
            int scale = 0;
            char typescl = 'G';
            int infoscl=0, kl=0, ku=0, drow=n, dcol=1, erow=n-1;
            double orgnrm = 0.0;
            if (scale) {
                char norm = 'M';
                orgnrm = lanst_wrap(&norm, &n, D, E);
                //double orgnrm = mylanst(&norm, &n, D, E);
                cout << "Norm " << orgnrm << endl;

                //cerr << "D before scale: " << endl ;
                //writeresult("out", D, 1, n);

                lascl_wrap(&typescl, &kl, &ku, &orgnrm, &one, &drow, &dcol, D, &drow, &infoscl);
                if(infoscl) {
                    cerr << "[Error] lascl_wrap on D failed! ret error " << infoscl << endl;
                    return infoscl;
                }

                //cerr << "D after  scale: " << endl ;
                //writeresult("out", D, 1, n);

                lascl_wrap(&typescl, &kl, &ku, &orgnrm, &one, &erow, &dcol, E, &erow, &infoscl);
                if(infoscl) {
                    cerr << "[Error] lascl_wrap on E failed! ret error " << infoscl << endl;
                    return infoscl;
                }
                ////
            }

            //for(int i=0; i<n; i++) {
            //    eigval[i]=D[i];
            //}
            D[cutpnt-1] = D[cutpnt-1] - fabs(rho);
            D[cutpnt]   = D[cutpnt]   - fabs(rho);

            QRTD(D,          q1, E,           n/2, ldz);
            QRTD(&D[cutpnt], q2, &E[cutpnt],  n/2, ldz);

            TYPE * workDC = new TYPE[n*n+4*n];
            if(int ret = ComputeEig(D, Z, workDC, q1, q2, rho, cutpnt, n-cutpnt, ldz)){
                cerr << "[Error] ComputeEig failed! ret error " << ret << endl;
                return ret;
            }
            delete [] workDC;
            delete [] tmp;

            // Scale back
            if (scale) {
                lascl_wrap(&typescl, &kl, &ku, &one, &orgnrm, &drow, &dcol, D, &drow, &infoscl);
                if(infoscl) {
                    cerr << "[Error] lascl_wrap scale back on D failed! ret error " << infoscl << endl;
                    return infoscl;
                }
            }

        } else {
            lapack_int lwork = 1 + (4*n) + (n*n);
            TYPE * work = new TYPE[lwork];
            lapack_int liwork = 3+(5*n);
            lapack_int * iwork = new lapack_int[liwork];
            lapack_int ret = LAPACKE_dstedc_work(LAPACK_COL_MAJOR, 'I', n, D, E ,Z, ldz, 
                    work, lwork, iwork, liwork);
            delete [] work;
            delete [] iwork;
            if(ret) {
                cerr << "[Error] dstedc failed! ret error " << ret  << endl;
            }
            //QRTD(D,          Z, E,           n, ldz);
        }
        //delete []q1;
        //delete []q2;
        //delete []wSize;
        //delete []wSize;
        ///////////////

        //
        // Apply Q to find A eigenvectors.   - ormtr
        //
        lwork = -1;
        work = new TYPE[1];
        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;
        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr II failed!" << endl;
            return ret;
        }
        t_end = rtclock();
        t_it[it] = t_end - t_start;
        ///////////////

#ifdef DEBUG
        cerr << "Eigenvalues: " << endl ;
        writeresult("out", D, 1, n);
        cerr << "Eigenvectors:" << endl;
        writeresult("out", Z, n, n);
#endif

        // Eval just a portion of eigenvectors
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, neigeneval, n, 1.0, Aread, n, Z, n, 1.0, LHS, n);

#ifdef DEBUG
        cerr << "LHS:" << endl;
        writeresult("out", LHS, n, n);
#endif
        //cout << "Aread:" << endl;
        //writeresult("out", Aread, n, n);
        for(int i = 0; i < neigeneval; i++) {
            cblas_daxpy(n, D[i], &Z[i*n], 1, &RHS[i*n], 1);
        }

        //Transpose the RHS
        transpose(RHS, RHStransp, n, n);
#ifdef DEBUG
        cerr << "RHStransp:" << endl;
        writeresult("out", RHStransp, n, n);
#endif

        incorrect = allclose(LHS, RHStransp, n, n);
        if (incorrect) {
            cerr << "[Error] Incorret result!!";
            break;
        } else {
            cout << " " << t_it[it]*1.0e3;
        }
    }
    cout << endl;

    delete [] t_it;
    delete [] RHS;
    delete [] RHStransp;
    delete [] LHS;
    delete [] Aread;
    delete [] A;
    delete [] Z;
    delete [] D;
    delete [] E;
    delete [] tau;
    delete [] work;

    return incorrect;
}
