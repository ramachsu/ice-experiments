//#include <lapacke.h>
#include "util.h"

using namespace std;

int main(int argc, char** argv)
{
/*
 *  Calculate eigenproblem n x n symmetric matrix
 */
    int ninp = 0;
    int niterations = 5;
    float checkrate = 1.0;
    string inpname = "N/A";

    if (argc >= 3) {
        ninp = std::stoi(argv[1]);
        inpname = argv[2];
        if (argc >= 4) {
            checkrate = std::stof(argv[3]);
        }
    } else {
        std::cerr << "Usage: ./<bin> <dim> <file path> [<checkrate of results>]" << endl;
        return 1;
    }
    int neigeneval = (int) ninp*checkrate;
    std::cout << "Symmetric Eigenproblem n: " << ninp << " inp: " << inpname << 
        " checking rate: " << checkrate << " (" << neigeneval << " eigenvalues and vectors)" << std::endl;

    lapack_int n=ninp, lda=n, ldc=n, ldz=n, lwork;
    TYPE * Aread = new TYPE[n*n];
    TYPE * A = new TYPE[n*n];
    TYPE * Z = new TYPE[n*n];
    TYPE * D = new TYPE[n];
    TYPE * E = new TYPE[n-1];
    TYPE * tau = new TYPE[n]; //new TYPE[n-1];
    TYPE * work = new TYPE[1];
    char uplo = 'U';
    char trans = 'N';
    char side = 'L';

    double t_start, t_end;
    double * t_it = new double[niterations];
    int incorrect = false;
    TYPE* LHS= new TYPE[n*n];
    TYPE* RHS= new TYPE[n*n]; 
    TYPE* RHStransp = new TYPE[n*n]; 

    if(int ret = readfile(inpname, Aread, ninp)) return ret;

#ifdef DEBUG
    cout << "Aread:" << endl;
    writeresult("out", Aread, n, n);
#endif
#ifdef DEBUG
    cout << "Starting lapack..." << endl;
#endif

    cout << "Running Bisection LAPACKE_dstebz_work LAPACKE_dstein_work" << endl;
    cout << "Eigproblem= "<< n << " Iterations= " << niterations << " Time(milisec)=";
    for (int it = 0; it < niterations; it++) {

        int index = 0;
        // Pass the data in column major
        for(int j=0; j < n; j++) {
            for(int i=0; i < n; i++ ) {
                //cout << "A[" << index << "] = " << Aread[i*n+j]  << endl;
                A[index++] = Aread[i*n+j];
            }
        }

        //
        // Calculate the T = QAQ', tridiagonal matrix
        // 
        t_start = rtclock();
        lwork = -1;
        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;

        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dsytrd_work(LAPACK_COL_MAJOR, uplo, n, A, lda, D, E, tau, work, lwork)) {
            cerr << "[Error] dsytrd II failed!" << endl;
            return ret;
        }

        delete [] work;
        ///////////////

        //
        // Call Bisection 
        //
        //
        if(lapack_int ret = Bisection(D, Z, E, n, ldz)) {
            cerr << "[Error] Bisection failed! ret code: " << ret << endl;
            return ret;
        }
        ///////////////

        //
        // Apply Q to find A eigenvectors.   - ormtr
        //
        lwork = -1;
        work = new TYPE[1];
        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr failed!" << endl;
            return ret;
        }

        lwork=work[0];
        delete [] work;
        work = new TYPE[lwork];

        if(lapack_int ret = LAPACKE_dormtr_work(LAPACK_COL_MAJOR, side, uplo, trans, n, n, A, lda, tau, Z, ldc, work, lwork)) {
            cerr << "[Error] dormtr II failed!" << endl;
            return ret;
        }
        t_end = rtclock();
        t_it[it] = t_end - t_start;
        ///////////////

#ifdef DEBUG
        cerr << "Eigenvalues: " << endl ;
        writeresult("out", D, 1, n);
        cerr << "Eigenvectors:" << endl;
        writeresult("out", Z, n, n);
#endif

        // Eval just a portion of eigenvectors
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, n, neigeneval, n, 1.0, Aread, n, Z, n, 1.0, LHS, n);

#ifdef DEBUG
        cerr << "LHS:" << endl;
        writeresult("out", LHS, n, n);
#endif
        //cout << "Aread:" << endl;
        //writeresult("out", Aread, n, n);
        for(int i = 0; i < neigeneval; i++) {
            cblas_daxpy(n, D[i], &Z[i*n], 1, &RHS[i*n], 1);
        }

        //Transpose the RHS
        transpose(RHS, RHStransp, n, n);
#ifdef DEBUG
        cerr << "RHStransp:" << endl;
        writeresult("out", RHStransp, n, n);
#endif

        incorrect = allclose(LHS, RHStransp, n, n);
        if (incorrect) {
            cerr << "[Error] Incorret result!!";
            break;
        } else {
            cout << " " << t_it[it]*1.0e3;
        }
    }
    cout << endl;

    delete [] t_it;
    delete [] RHS;
    delete [] RHStransp;
    delete [] LHS;
    delete [] Aread;
    delete [] A;
    delete [] Z;
    delete [] D;
    delete [] E;
    delete [] tau;
    delete [] work;

    return incorrect;
}
