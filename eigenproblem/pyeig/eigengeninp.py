#!/usr/bin/env python

import numpy as np
import argparse
from numpy import linalg as LA

parser =  argparse.ArgumentParser(description="Generate symmetric n x n matrix for the eigenproblem.")
parser.add_argument("-n", help="Dimension of the symmetric matrix.", type=int, required=True)
parser.add_argument("-o", help="Name of the file to output matrix.", type=str, default="outmat")
args = parser.parse_args()

#w, v = LA.eig(np.diag((1, 2, 3)))
N = args.n
b = np.random.random_integers(-2000,2000, size=(N,N))
b_symm = (b + b.T)
#print(b_symm)
outfilename = args.o+"_"+str(args.n)+".inp"
np.savetxt(outfilename, b_symm, fmt="%d")
print(f"Saved in file {outfilename}.")



