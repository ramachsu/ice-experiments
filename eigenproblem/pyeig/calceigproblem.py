#!/usr/bin/env python

import numpy as np
import argparse
from numpy import linalg as LA

parser =  argparse.ArgumentParser(description="Calculate eigenvector and eigenvalues of a symmetric n x n matrix.")
#parser.add_argument("-n", help="Dimension of the symmetric matrix.", type=int, required=True)
parser.add_argument("--inpfile", help="Name of the file to output matrix.", type=str, required=True)
args = parser.parse_args()

#w, v = LA.eig(np.diag((1, 2, 3)))
#print(w)
#print(v)

#N = args.n
#b = np.random.random_integers(-2000,2000, size=(N,N))
#b_symm = (b + b.T)
#print(b_symm)
#outfilename = args.o+"_"+str(args.n)+".inp"
#np.savetxt(outfilename, b_symm, fmt="%d")
#print(f"Saved in file {outfilename}.")

# Read the file

with open(args.inpfile, 'r') as f:
    matA = np.loadtxt(f)

#print(matA)
w, v = LA.eig(matA)
print(f"Eigenvalues: {w}")

print(f"Eigenvector(s): ")
for i,eigvec in enumerate(v.T):
    print(f"{i}) {eigvec}")
##
print("====\nChecking ...")
for i, (eigval, eigvec) in enumerate(zip(w.T,v.T)):
    LHS = np.dot(matA, eigvec)
    RHS = np.dot(eigval, eigvec)
    check=np.allclose(LHS,RHS)
    print(f"{i}) {check} !")
    #print(f"{LHS}")
#endfor




