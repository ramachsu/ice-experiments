#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <pips_runtime.h>

#include <assert.h>

#define N 2000
#define T 1000

//#pragma declarations
double a[2][N+2][N+2];
//double b[N][N];
//#pragma enddeclarations

//#ifdef TIME
#define IF_TIME(foo) foo;
//#else
//#define IF_TIME(foo)
//#endif

void init_array()
{
    int i, j;

    for (i=0; i<N+2; i++) {
        for (j=0; j<N+2; j++) {
            a[0][i][j] = ((double)j)/N;
        }
    }
}


void print_array()
{
    int i, j;

    for (i=1; i<N+1; i++) {
        for (j=1; j<N+1; j++) {
            fprintf(stderr, "%lf ", a[T%2][i][j]);
            if (j%80 == 20) fprintf(stderr, "\n");
        }
    }
    fprintf(stderr, "\n");
}

double rtclock()
{
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, NULL);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#define __PLACE_TO_INSERT_FORWARD_DECLARATIONS

int main()
{
    int t, i, j;
    double t_start, t_end;

    init_array();

    IF_TIME(t_start = rtclock());

//#pragma scop
#pragma @ICE loop=jacobi2d
    for (t=0; t<T; t++) {
        for (i=1; i<N+1; i++) {
            for (j=1; j<N+1; j++) {
                a[(t+1)%2][i][j]= 0.2*(a[t%2][i][j]+a[t%2][i][j-1]+a[t%2][i][1+j]+a[t%2][1+i][j]+a[t%2][i-1][j]);
            }
        }
    }
//#pragma endscop

    IF_TIME(t_end = rtclock());
    IF_TIME(fprintf(stdout,"Jacobi2d size = %d steps = %d | Time = %7.5lf ms\n", N,T,(t_end - t_start)*1.0e3))
//    IF_TIME(fprintf(stdout, "%0.6lfs\n", t_end - t_start));

    if (fopen(".test", "r")) {
#ifdef MPI
        if (my_rank == 0) {
            print_array();
        }
#else
        print_array();
#endif
    }

    return 0;
}
