#!/bin/bash

comp=icc
mach=gienah
origdimZ=1600
origdimX=$origdimZ
origdimY=$origdimZ

#dim=256L
dimZ=$origdimZ
dimY=$origdimY
dimX=$origdimX

#for nproc in 1 10 20
for nproc in 10 20
do
	dimZ=$((${origdimZ}/${nproc}))
	make CC=${comp} USERDEFS="mysecond.c -DNZ=${dimZ}L -DNY=${dimY}L -DNX=${dimX}L " clean orig
	seq ${nproc} | parallel taskset -c '$((2*({%}-1)))' ./orig &> result-orig-${origdimZ}-ZYX-total-${dimZ}-Z-${mach}-${comp}-parallel-${nproc}.log

done
