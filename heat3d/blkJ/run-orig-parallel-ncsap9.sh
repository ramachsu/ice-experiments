#!/bin/bash

comp=xlc
mach=p9 #${mach}
origdimZ=3200
origdimX=$origdimZ
origdimY=$origdimZ

#dim=256L
dimZ=$origdimZ
dimY=$origdimY
dimX=$origdimX
nproc=1
#make CC=${comp} USERDEFS="mysecond.c -DNZ=${dimZ}L -DNY=${dimY}L -DNX=${dimX}L" clean orig
#./orig &> result-orig-${dim}-${mach}-${comp}-parallel-${nproc}.log

for nproc in 20 40 80
do

dimZ=$((${origdimZ}/${nproc}))
make CC=${comp} USERDEFS="mysecond.c -DNZ=${dimZ}L -DNY=${dimY}L -DNX=${dimX}L" clean orig

vexpr=$((80/${nproc}))
exp="'\$((({%}-1)*${vexpr}))'"
seq ${nproc} | parallel taskset -c "'$exp'" ./orig &> result-orig-${origdimZ}-ZYX-total-${dimZ}-Z-${mach}-${comp}-parallel-${nproc}.log

#seq ${nproc} | parallel taskset -c '$((({%}-1)*4))' ./orig &> result-orig-${origdim}-total-${dim}-${mach}-${comp}-parallel-${nproc}.log

#dimZ=$((${origdim}/${nproc}))
#make CC=${comp} USERDEFS="mysecond.c -DNZ=${dimZ}L -DNY=${dimY}L -DNX=${dimX}L" clean orig
#seq ${nproc} | parallel taskset -c '$((({%}-1)*2))' ./orig &> result-orig-${origdim}-total-${dim}-${mach}-${comp}-parallel-${nproc}.log

#dimZ=$((${origdim}/${nproc}))
#make CC=${comp} USERDEFS="mysecond.c -DNZ=${dimZ}L -DNY=${dimY}L -DNX=${dimX}L" clean orig
#seq ${nproc} | parallel taskset -c '$(({%}-1))' ./orig &> result-orig-${origdim}-ZYX-total-${dim}-${mach}-${comp}-parallel-${nproc}.log
done
