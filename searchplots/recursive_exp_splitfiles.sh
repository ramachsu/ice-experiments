#!/usr/bin/env bash

inputfile=$1 #experiment-already-expandtree-approx75-nofpprecise-202003311123.txt
csplit $inputfile /Searching/ {*} -f ${inputfile%.*} -b "_split_%02d.log" -z


for f in `ls ${inputfile%.*}_split_*.log`; do 
    #echo $f;
    #echo "${f%.*}";
    #mv -v $f ${f%.*}_hyperopt.${f##*.};
    nfoundshypopt=`awk '/Searching/ && /hyperopt/{print "Hyperopt " $0}' $f | wc -l`
    nfoundsoptuner=`awk '/Searching/ && /opentuner/{print "Opentuner " $0}' $f | wc -l`
    if [ $nfoundshypopt -gt 0 ]; then
        mv $f ${f%_*}_hyperopt_${f##*_}
    elif [ $nfoundsoptuner -gt 0 ]; then 
        mv $f ${f%_*}_opentuner_${f##*_}
    fi
done

#rm -f ${inputfile%.*}_split_*.log
