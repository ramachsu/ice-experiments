#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse
import math
import matplotlib.pyplot as plt
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, required=True, help="Input csv file path.")
parser.add_argument('-o', type=str, required=True, help="Output pdf file name.")
#parser.add_argument('-p', type=int, required=False, default=1,
#        help="Which chart to plot? 1: per_opt 2: wallclk "+
#             "cacheoblvi 3 per_flops 4")

args=parser.parse_args()

linestyles=["-","--","-.",":"]
markerstyles=[".","+","x",'s']

def plottime(df) :
    strgs = df.groupby('leg') #df['leg'].unique()
    linesty=itertools.cycle(linestyles)
    mksty  =itertools.cycle(markerstyles)

    for key,st in strgs:
        #print(st)
        x = st['len'].unique()
        #print("x",x)
        #y = st.groupby('len').median().time/1e3
        y = st.groupby('len').min().time/1e3
        #ymin = st.groupby('len').min().time/1e3
        #ymax = st.groupby('len').max().time/1e3
        print(key, y)
        label = key #st.leg

        #plt.errorbar(x, y, yerr=[y-ymin, ymax-y], label=label, capsize=2, marker=next(mksty), linestyle=next(linesty))
        plt.plot(x, y, label=label, marker=next(mksty), linestyle=next(linesty), fillstyle='none')

        #x = df['len'].unique()
        #y = df.groupby('len').median().time #['time'] #[['time']]
        #ymin = df.groupby('len').min().time
        #ymax = df.groupby('len').max().time
        #print(f"{type(y)}\n{y}\n{x}")
    ##
    ticks=df['len'].unique()

    #leg=str(size)
    #plt.errorbar(x, y, yerr=[ymin, ymax], label=leg, capsize=2)
    plt.title("Eigenproblem on Stellar machine")
    plt.xticks(ticks)
    plt.tick_params(axis='x', labelsize=8, labelrotation=40.0)
    plt.xlabel("N")
    plt.ylabel("Time (sec)")
    plt.legend(loc='upper left')
    figname=args.o
    plt.savefig(figname)
    print(f"Saved chart in {figname} .")
##


def filterTechnique(tec):
    if tec.startswith('mix'):
        return "Hyperopt Rand+Anneal+TPE"
    elif tec.startswith('bandit'):
        return "Opentuner"
    elif tec.startswith('tpe'):
        return "Hyperopt TPE"
    else:
        return tec
###

def filterinitpnt(ipnt):
    if ipnt.startswith("w2"):
        return "- Initial point 2 level tiling"
    elif ipnt.startswith("w3"):
        return "- Initial point 3 level tiling"
    else:
        return ipnt
###

def plotSearch(df):
    nrows = 2
    ncols = 2
    fig, axes = plt.subplots(nrows, ncols, figsize=(15,9), sharey='row') 

    sbrowidx = 0
    sbcolidx = 0
    #bycc = initgrp.groupby('comp')
    bycc = df.groupby('comp')
    for cckey, ccgrp in bycc:
        print(f"Processing {cckey} {sbrowidx} {sbcolidx}")
        #byinitpnt = df.groupby('initpnt')
        byinitpnt = ccgrp.groupby('initpnt')
        for initkey, initgrp in byinitpnt:
            print(f"Processing {initkey}")
            linsty =itertools.cycle(linestyles)
            mksty  =itertools.cycle(markerstyles)

            pltobj = axes[sbrowidx][sbcolidx]
            title = cckey+' '+filterinitpnt(initkey)
            #bytech = ccgrp.groupby('stech')
            bytech = initgrp.groupby('stech')
            for teckey, tecgrp in bytech:
                print(f"Processing {teckey}")
                leg = filterTechnique(teckey)
                x = tecgrp['stime'].unique()
                ygrp = tecgrp.groupby('stime')
                y = ygrp.metric.median()/1e3
                ymax = (ygrp.metric.max()/1e3)-y
                ymin = y-(ygrp.metric.min()/1e3)
                #print(f"{x} {y}")
                #npnts = len(y)
                pltobj.errorbar(x,y,yerr=[ymin, ymax], label=leg, capsize=2,
                #pltobj.plot(x, y, label=leg,
                            marker=next(mksty), linestyle=next(linsty), 
                            fillstyle='none', elinewidth=0.4, errorevery=(0,3))
            ###
            if sbcolidx == ncols-1 and sbrowidx == 0:
                lgd = pltobj.legend(loc='upper right', ncol=1)
            #
            if sbcolidx == 0:
                pltobj.set_ylabel('Execution Time (seconds)')
            #
            if sbrowidx == nrows-1:
                pltobj.set_xlabel("Search time (sec)")
            #
            pltobj.set_title(title)
            sbcolidx += 1
        ###
        sbrowidx += 1
        sbcolidx = 0
    ###
    plt.savefig(args.o,
            bbox_extra_artists=(lgd,),
            bbox_inches='tight')
    print(f"Save figure as {args.o}")
###

df = pd.read_csv(args.i, comment="#")
print(df)
print(f"Reading file {args.i} ...")
#print(f"Plot selected {args.p}!")

#df.sort_values(['len'], ascending=True, inplace=True)

#plottime(df)
plotSearch(df)


