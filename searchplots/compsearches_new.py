#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import sys


parser = argparse.ArgumentParser()
#parser.add_argument('--t', type=str, required=True, help="Title for the chart")
#parser.add_argument('--o', type=str, required=False, default="defaul.csv",help="Output csv name")
parser.add_argument('--files',  nargs='+', required=False, help="Receive a list of files.")
#parser.add_argument('--filesB',  nargs='+', required=False, help="Receive a list of files.")
parser.add_argument('--interval', type=int, required=False, default=60,help="Interval in seconds")
parser.add_argument('--limit', type=int, required=False,
        default=3600,help="Chart limit in seconds")
parser.add_argument('--lowP', type=int, required=False, default=None, help="Lower Percentile [0,50].")
parser.add_argument('--upP', type=int, required=False, default=None, help="Upper Percentile [50,100].")
parser.add_argument('--comp', type=str, default="na",help="Compiler used")
parser.add_argument('--stool', type=str, default="na",help="Search tool used")
parser.add_argument('--stech', type=str, default="na",help="Search technique used")
parser.add_argument('--desc', type=str, default="na",help="Description about experiment/strategy")
parser.add_argument('--initpnt', type=str, default="na",help="Init point of the search")
args = parser.parse_args()


def genCSVperfilep(files):
    pltx = []
    plty = []

    if args.lowP and (args.lowP < 0 or args.lowP > 50):
        print("ERROR! lower percentitle must be between 0 and 50.")
        return
    #endig
    if args.upP and (args.upP < 50 or args.upP > 100):
        print("ERROR! upper percentitle must be between 50 and 100.")
        return
    #endif

    #minlen = sys.maxsize
    rngelem = range(args.interval, args.limit, args.interval)
    numelem = len(rngelem)
    lstresx = list(rngelem)

    #wfile.write("x,"+",".join([str(x) for x in lstresx])+"\n")
    for i, inpfile in enumerate(files):
        print ("Processing ", i,": ", inpfile,  file=sys.stderr)
        data = []
        with open(inpfile, 'r') as fi:
            duration = int(next(fi).split()[3])
            for line in fi:
                v = line.split()
                data.append((int(v[0]), float(v[1])))
        #end with

        xinp = [v[0] for v in data]
        yinp = [v[1] for v in data]

        resdic = {}

        #for i in range(0, args.limit, args.interval):
        for inte in rngelem:
            for ixp, iyp in zip(xinp, yinp):
                if ixp < inte and (inte not in resdic or iyp < resdic[inte]):
                    resdic[inte] = iyp
                    #print ("resdic[",inte,"] = ", iyp)
            #endfor
        #endfor

        # if input data has values starting out the range
        if not resdic and yinp:
            for inte in rngelem:
                resdic[inte] = yinp[0]
        #endif

        if resdic:
            lstresy = [resdic[ty] for ty in sorted(resdic)]
            if len(lstresy) < numelem:
                misselem = numelem - len(resdic)

                # expand to the left so the arrays have
                # the same length
                tmp = [lstresy[0]]*misselem
                tmp.extend(lstresy)
                lstresy = tmp
            #endif
            #print ("lstresy: ",lstresy)

            pltx.append(lstresx)
            plty.append(lstresy)
            #wfile.write("y"+str(i)+","+",".join([str(y) for y in lstresy])+"\n")
            #print (len(lstresy), lstresy)
            #print (pltx[-1:],"----",plty[-1:])
        else:
            print ("resdic has no values for this range on this file!")
        #endif

        #plt.plot(xinp, yinp, label=leg)
    #endfor
    #endwith

    #xarr = np.array([np.array(k[-minlen:]) for k in pltx])
    #yarr = np.array([np.array(k[-minlen:]) for k in plty])
    xarr = np.array([np.array(k) for k in pltx])
    yarr = np.array([np.array(k) for k in plty])
    #print (plty)

    if args.upP:
        upperP = np.percentile(yarr, args.upP, axis=0)
        #print (args.upP,"upper Percentile:", upperP)
    #endif

    if args.lowP:
        lowerP = np.percentile(yarr, args.lowP, axis=0)
        #print (args.lowP,"lower Percentile:", lowerP)
    #endif


    yarrTmp = yarr.copy()
    if args.upP:
        yarrTmp = yarrTmp.clip(max=upperP)
    #endif

    if args.lowP:
        yarrTmp = yarrTmp.clip(min=lowerP)
    #endif

    return xarr[0], yarrTmp
###

if args.files:
    x,y = genCSVperfilep(args.files)
    #print("Save the data in ", args.o)
    print(f"shape: x: {np.shape(x)} y: {np.shape(y)}", file=sys.stderr)
    #print(f"x: {x} y: {y}")
    rows, cols = np.shape(y)

    fo = sys.stdout
    #with open(args.o, 'w') as fo:
    fo.write('leg,comp,stool,stech,initpnt,stime,metric\n')
    # gen a csv with one yvalue per row
    # needs a transpose
    for yrow in y:
        for i, yval in enumerate(yrow):
            fo.write(f"{args.desc},{args.comp},{args.stool},{args.stech},{args.initpnt},{x[i]},{yval}\n")
        ##
    ##
    ##
#





