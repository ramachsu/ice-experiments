#!/bin/bash


#grdate="grep '\[201[89]-[0-9]' $fileinp | head -1 | awk '{print $1 " " $2}' | sed 's/\[//g' | sed 's/\]//g'"
funcgrinitdate () {
	#echo $fileinp
	#grline="grep '201[89]-[0-9][0-9]-[0-9][0-9]' ${fileinp} "
	grline="grep '201[89]-[0-9][0-9]-[0-9][0-9]' "
	#grdate="awk '{print \$3 \" \"  \$4}' | sed 's/\[//g' | sed 's/\]//g'"
	grdate="awk '{print \$1 \" \"  \$2}' | sed 's/\[//g' | sed 's/\]//g'"
	#grperf="awk '\$20!~/inf/{print \$20}'"
	#grperf="awk '{print \$20}'"
	grperf="awk '{print \$18}'"
}

datediff() {
    d1=$(date -d "$1" +%s)
    d2=$(date -d "$2" +%s)
    #echo $1 $2 $d1 $d2 $(($d1 - $d2))
    seconds=$(( (d1 - d2) ))
    minutes=$(( $seconds / 60 )) #`date -d @$((  $d1 - $d2 / 60 )) -u +%s`
}

for fileinp in "$@";
do
funcgrinitdate
#initdate=`head -1 $fileinp | eval "$grline" | eval "$grdate" `
initdate=`eval "$grline $fileinp" | head -1 | eval "$grdate" `
#enddate=`tail -1 $fileinp | eval "$grline" | eval "$grdate" `
enddate=` eval "$grline $fileinp" | tail -1 | eval "$grdate" `
datediff  "$enddate" "$initdate"
echo "experiment duration (sec): " $seconds
#eval $grdate
	lastda=$initdate
	minperf=1.0*10^100
	minutestart=0
	grep "Variant result" $fileinp | eval $grline |  while read -r line ; do
		#echo $line #| eval $grperf
		currda=`echo $line | eval "$grdate"`
		currperf=`echo $line | eval "$grperf"`
		#diffda=`date -d @$(( $(date -d "$currda" +%s) - $(date -d "$initdate" +%s) )) -u +'%H:%M:%S'`

		if [[ $currperf != "inf" ]]; then
			#echo "minperf " $minperf " currperf " $currperf
			if (( $(echo $minperf '>' $currperf | bc -l)  )); then
				#diffminutes=`date -d @$(( $(date -d "$currda" +%s) - $(date -d "$lastda" +%s) )) -u +'%M'`
				datediff "$currda" "$initdate"	
				minperf=$currperf
				echo $seconds $minperf
			fi
		fi

		#datediff "$currda" "$lastda"	
		#echo ">>>> " $currda $currperf $minutes $seconds 
		#if [[ $minutestart -eq 0 ]]; then
		#	#echo $minutes $minperf
		#	echo $seconds $minperf
		#	#minutestart=$(($minutes + 1))
		#	minutestart=$(($seconds + 1))
		#else
		#	#limitminutes=$(( $minutestart + $minutes - 1 ))
		#	limitminutes=$(( $minutestart + $seconds - 1 ))
		#	#for m in `seq $minutestart 1 $limitminutes`; do
		#	for m in `seq $minutestart 60 $limitminutes`; do
		#		echo $m $minperf
		#	done
		#	#minutestart=$(($limitminutes + 1))
		#	minutestart=$(($limitminutes + 1))
		#fi
		#lastda=$currda
	done
done
