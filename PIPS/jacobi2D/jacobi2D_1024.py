#!/usr/bin/env python

import sys, os, shutil

from pyps import *

def findLoopWithLabel(loops, label):
        for l1 in loops:
            if l1.label == label:
                return l1
            else:
                ret = findLoopWithLabel(l1.loops(), label)
                if ret:
                    return ret
        return None


# removing previous output
if os.path.isdir("jacobi2D_1024.database"):
    shutil.rmtree("jacobi2D_1024.database", True)

# RECIPE PREAMBLE (FILE,MODULE)

ws = workspace("jacobi2D_1024_orig.c" , name="jacobi2D_1024",deleteOnClose=True)
ws.activate("MUST_REGIONS")
ws.activate("RICE_REGIONS_DEPENDENCE_GRAPH")
ws.props.MEMORY_EFFECTS_ONLY=False
ws.props.PRETTYPRINT_DIV_INTRINSICS=False
fct = ws.fun.jacobi2D

loop1 = findLoopWithLabel(fct.loops(),"loop0")

# RECIPE GENERAL TILING (loop0,mat="1 0 0 , 1 32 32,1 -32 32")

loop1.loop_tiling (LOOP_TILING_MATRIX = "1 0 0 , 1 32 32,1 -32 32")

 ## RECIPE_SIMPLIFICATION++
# with Constant propagation
# 
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()

# RECIPE FLAGS
fct.flag_loops()
fct.display()

# RECIPE SELECT LOOP(l99996)
loop1 = findLoopWithLabel(fct.loops(),"l99996")

# RECIPE HORIZONTAL_UNROLL(l99996,2)
loop1.unroll(UNROLL_RATE=2)
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.flag_loops()
fct.display()
#loop1 = findLoopWithLabel(fct.loops(),"l99989")
fct.loop_fusion()
fct.scalarization()
fct.split_initializations()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()

# RECIPE CLEAN_LABELS
fct.clean_labels()
# RECIPE PARALLELIZATION 
fct.privatize_module()
fct.coarse_grain_parallelization()
fct.display ()
# RECIPE POSTAMBLE
ws.close()
