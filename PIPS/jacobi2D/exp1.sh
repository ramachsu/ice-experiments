export OMP_DISPLAY_ENV=true
export OMP_PROC_BIND=spread
export OMP_PLACES=cores
export OMP_NUM_THREADS=8
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_orig; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32_u2; done
export OMP_NUM_THREADS=16
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_orig; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32_u2; done
export OMP_NUM_THREADS=32
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_orig; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32_u2; done
export OMP_NUM_THREADS=40
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_orig; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32_u2; done
export OMP_NUM_THREADS=64
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_orig; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile2D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_16; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32; done
for i in 1 2 3 4 5;  do  ./jacobi2D_1024_tile3D_32_u2; done
