#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 20000
#endif

#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))

void mvt(int n, float a[N][N], float y[N], float x[N])
{
 int i,j;
    for(i = 0; i < n; i++)
      for(j = 0; j < n; j++)
	x[i] = x[i] + a[j][i] * y[j];

}
void mvt_seq(int n, float a[N][N], float y[N], float x[N])
{
  int i,j;

 loop0:
  for(j = 0; j < n; j++)
    for(i = 0; i < n; i++)
      x[i] = x[i] + a[j][i] * y[j];

}

void mvt_omp(int n, float a[N][N], float y[N], float x[N])
{
  int i,j;

 loop0:
  for(j = 0; j < n; j++)
#pragma omp parallel for private(i)
    for(i = 0; i < n; i++)
      x[i] = x[i] + a[j][i] * y[j];

}

void mvt_tile_8x8(int n, float a[20000][20000], float y[20000], float x[20000])
{
   int i, j;
   //PIPS generated variable
   int j_t, i_t;

   for(j_t = 0; j_t <= (n-1)/8; j_t += 1)
     //#pragma omp parallel for private(i,j)
      for(i_t = 0; i_t <= (n-1)/8; i_t += 1)
         for(j = 8*j_t; j <= pips_min(8*j_t+7, n-1); j += 1)
#pragma omp parallel for private(i)
            for(i = 8*i_t; i <= pips_min(8*i_t+7, n-1); i += 1)
               x[i] = x[i]+a[j][i]*y[j];
}

void mvt_tile_16x16(int n, float a[20000][20000], float y[20000], float x[20000])
{
   int i, j;
   //PIPS generated variable
   int j_t, i_t;

   for(j_t = 0; j_t <= (n-1)/16; j_t += 1)
      for(i_t = 0; i_t <= (n-1)/16; i_t += 1)
	for(j = 16*j_t; j <= pips_min( 16*j_t+15, n-1); j += 1)
#pragma omp parallel for private(i)
            for(i = 16*i_t; i <= pips_min(16*i_t+15, n-1); i += 1)
               x[i] = x[i]+a[j][i]*y[j];
}
void mvt_tile_32x32(int n, float a[20000][20000], float y[20000], float x[20000])
{
   int i, j;
   //PIPS generated variable
   int j_t, i_t;


   for(j_t = 0; j_t <= (n-1)/32; j_t += 1)
      for(i_t = 0; i_t <= (n-1)/32; i_t += 1)

         for(j = 32*j_t; j <= pips_min(32*j_t+31, n-1); j += 1)
#pragma omp parallel for private(i)
	   for(i = 32*i_t; i <= pips_min( 32*i_t+31, n-1); i += 1)
               x[i] = x[i]+a[j][i]*y[j];
}

void mvt_tile_8x8_uh4(int n, float a[20000][20000], float y[20000], float x[20000])
{
   //PIPS generated variable
   int j_t, i_t, LU_IND0, i4;

   for(j_t = 0; j_t <= 2499; j_t += 1)
#pragma omp parallel for private(LU_IND0,i4)
      for(i_t = 0; i_t <= 2499; i_t += 1)

         for(LU_IND0 = 0; LU_IND0 <= 7; LU_IND0 += 4)
	   //#pragma omp parallel for 
            for(i4 = 8*i_t; i4 <= 8*i_t+7; i4 += 1)
               x[i4] = x[i4]+a[LU_IND0+8*j_t][i4]*y[LU_IND0+8*j_t]+a[LU_IND0+8*j_t+1][i4]*y[LU_IND0+8*j_t+1]+a[LU_IND0+8*j_t+2][i4]*y[LU_IND0+8*j_t+2]+a[LU_IND0+8*j_t+3][i4]*y[LU_IND0+8*j_t+3];
}

void mvt_tile_16x16_uh4(int n, float a[20000][20000], float y[20000], float x[20000])
{
   //PIPS generated variable
   int j_t, i_t, LU_IND0, i4;

   for(j_t = 0; j_t <= 1249; j_t += 1)
#pragma omp parallel for private(LU_IND0,i4)
      for(i_t = 0; i_t <= 1249; i_t += 1)

         for(LU_IND0 = 0; LU_IND0 <= 15; LU_IND0 += 4)
	   //#pragma omp parallel for 
            for(i4 = 16*i_t; i4 <= 16*i_t+15; i4 += 1)
               //PIPS generated variable
               x[i4] = x[i4]+a[LU_IND0+16*j_t][i4]*y[LU_IND0+16*j_t]+a[LU_IND0+16*j_t+1][i4]*y[LU_IND0+16*j_t+1]+a[LU_IND0+16*j_t+2][i4]*y
[LU_IND0+16*j_t+2]+a[LU_IND0+16*j_t+3][i4]*y[LU_IND0+16*j_t+3];
}


void mvt_tile_32x32_uh4(int n, float a[20000][20000], float y[20000], float x[20000])
{
   //PIPS generated variable
   int j_t, i_t, LU_IND0, i4;

   for(j_t = 0; j_t <= 624; j_t += 1)
#pragma omp parallel for private(LU_IND0,i4)
      for(i_t = 0; i_t <= 624; i_t += 1)

         for(LU_IND0 = 0; LU_IND0 <= 31; LU_IND0 += 4)
	   //#pragma omp parallel for 
            for(i4 = 32*i_t; i4 <= 32*i_t+31; i4 += 1)
               //PIPS generated variable
               x[i4] = x[i4]+a[LU_IND0+32*j_t][i4]*y[LU_IND0+32*j_t]+a[LU_IND0+32*j_t+1][i4]*y[LU_IND0+32*j_t+1]+a[LU_IND0+32*j_t+2][i4]*y
[LU_IND0+32*j_t+2]+a[LU_IND0+32*j_t+3][i4]*y[LU_IND0+32*j_t+3];
}

void mvt_tile_32x32_uh8(int n, float a[20000][20000], float y[20000], float x[20000])
{
   //PIPS generated variable
   int j_t, i_t, LU_IND0, i8;

   for(j_t = 0; j_t <= 624; j_t += 1)
#pragma omp parallel for private(LU_IND0,i8)
      for(i_t = 0; i_t <= 624; i_t += 1)

         for(LU_IND0 = 0; LU_IND0 <= 31; LU_IND0 += 8)
	   //#pragma omp parallel for 
            for(i8 = 32*i_t; i8 <= 32*i_t+31; i8 += 1) 
                x[i8] = x[i8]+a[LU_IND0+32*j_t][i8]*y[LU_IND0+32*j_t]+a[LU_IND0+32*j_t+1][i8]*y[LU_IND0+32*j_t+1]+a[LU_IND0+32*j_t+2][i8]*y[LU_IND0+32*j_t+2]+a[LU_IND0+32*j_t+3][i8]*y[LU_IND0+32*j_t+3]+a[LU_IND0+32*j_t+4][i8]*y[LU_IND0+32*j_t+4]+a[LU_IND0+32*j_t+5][i8]*y[LU_IND0+32*j_t+5]+a[LU_IND0+32*j_t+6][i8]*y[LU_IND0+32*j_t+6]+a[LU_IND0+32*j_t+7][i8]*y[LU_IND0+32*j_t+7];

}


int main()
{
  float (*a)[N][N],(*x1)[N], (*y)[N];
  a=malloc((N)*(N)*sizeof(float));
  x1=malloc((N)*sizeof(float));
  y=malloc((N)*sizeof(float));
  int i,j,k, n=N;
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      (*a)[i][j]=.5;

  for(i=0;i<N;i++)
    (*x1)[i]=0,(*y)[i]=2;
  
  printf("calcul de Mvt_orig\n");
 
  gettimeofday (&tvStart,NULL);
  mvt_omp(N,*a,*y,*x1);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  return 0;
}
