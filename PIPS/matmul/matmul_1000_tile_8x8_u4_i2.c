#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 1000
#endif

#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))
#define  MOD(x,y) (x%y)
void Matrix_Mult(float a[N][N], float b[N][N], float c[N][N])
{
  int i, j, k;

 loop0:
  for(j = 0; j <N; j ++)
  loop1:
    for(k = 0; k <N; k ++)
    loop2:
      for(i = 0; i <N; i ++) {
 	c[i][j] = c[i][j]+a[i][k]*b[k][j];
      }
  
}


  
void Matrix_Mult_tile_8x8_u4_i2(float a[1000][1000], float b[1000][1000], float c[1000][1000])
{
   //PIPS generated variable
   int i_t, k_t, LU_IND0, j4, LU_IND1;

#pragma omp parallel for private(i_t,LU_IND1,k_t,LU_IND0,j4)
   for(i_t = 0; i_t <= 124; i_t += 1)
      for(k_t = 0; k_t <= 124; k_t += 1) {


	//#pragma omp parallel for private(LU_IND0,j4)
	for(LU_IND1 = 0; LU_IND1 <= 7; LU_IND1 += 2)
	  for(LU_IND0 = 0; LU_IND0 <= 7; LU_IND0 += 4) {
	    //#pragma omp parallel for 
	    for(j4 = 0; j4 <= 999; j4 += 1)
	      c[LU_IND1+8*i_t][j4] = c[LU_IND1+8*i_t][j4]+a[LU_IND1+8*i_t][LU_IND0+8*k_t]*b[LU_IND0+8*k_t][j4]+a[LU_IND1+8*i_t][LU_IND0+8*k_t+1]*b[LU_IND0+8*k_t+1][j4]+a[LU_IND1+8*i_t][LU_IND0+8*k_t+2]*b[LU_IND0+8*k_t+2][j4]+a[LU_IND1+8*i_t][LU_IND0+8*k_t+3]*b[LU_IND0+8*k_t+3][j4];
	    //#pragma omp parallel for 
	    for(j4 = 0; j4 <= 999; j4 += 1)
	      c[LU_IND1+8*i_t+1][j4] = c[LU_IND1+8*i_t+1][j4]+a[LU_IND1+8*i_t+1][LU_IND0+8*k_t]*b[LU_IND0+8*k_t][j4]+a[LU_IND1+8*i_t+1][LU_IND0+8*k_t+1]*b[LU_IND0+8*k_t+1][j4]+a[LU_IND1+8*i_t+1][LU_IND0+8*k_t+2]*b[LU_IND0+8*k_t+2][j4]+a[LU_IND1+8*i_t+1][LU_IND0+8*k_t+3]*b[LU_IND0+8*k_t+3][j4];
            }
      }
}

 

int main()
{
  float (*a)[N][N],(*b)[N][N],(*c)[N][N];
  a=malloc((N)*(N)*sizeof(float));
  b=malloc((N)*(N)*sizeof(float));
  c=malloc((N)*(N)*sizeof(float));
  int i,j,k, n=N;
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      (*a)[i][j]=.5, (*b)[i][j]=.5,(*c)[i][j]=0;
  printf("calcul de Matrix_Mult_tile_8x8_u4_i2\n");
  gettimeofday (&tvStart,NULL);
  Matrix_Mult_tile_8x8_u4_i2(*a,*b,*c);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  return 0;
}
