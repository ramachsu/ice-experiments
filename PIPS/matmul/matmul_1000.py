#!/usr/bin/env python

import sys, os, shutil

from pyps import *

def findLoopWithLabel(loops, label):
        for l1 in loops:
            if l1.label == label:
                return l1
            else:
                ret = findLoopWithLabel(l1.loops(), label)
                if ret:
                    return ret
        return None


# removing previous output
if os.path.isdir("matmul_1000_2.database"):
    shutil.rmtree("matmul_1000_2.database", True)

# RECIPE PREAMBLE (matmul_1000, Matrix_Mult)

ws = workspace("matmul_1000.c" , name="matmul_1000_2",deleteOnClose=True)
ws.activate("MUST_REGIONS")
ws.activate("RICE_REGIONS_DEPENDENCE_GRAPH")
ws.props.MEMORY_EFFECTS_ONLY=False
ws.props.PRETTYPRINT_DIV_INTRINSICS=False
fct = ws.fun.Matrix_Mult
fct.display ()

# RECIPE SELECT LOOP(loop0)
fct.flag_loops()
loop1 = findLoopWithLabel(fct.loops(),"loop0")
fct.display()

# RECIPE RECTANGULAR TILING (2,8,8)
loop1.loop_tiling (LOOP_TILING_MATRIX = "8 0,0 8")
fct.display ()

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.display ()

# RECIPE UNROLL_JAM("l99998",4)
fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"l99998")
loop3.unroll(UNROLL_RATE=4)
fct.display ()
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.loop_fusion()
fct.display ()

# RECIPE_AGGREGATION (4)
fct.scalarization()
fct.split_initializations()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.forward_substitute()
fct.simplify_control()
fct.dead_code_elimination()
fct.loop_fusion()
fct.display ()  


# RECIPE UNROLL_JAM("l99997",2)
fct.flag_loops()
fct.display ()
loop3 = findLoopWithLabel(fct.loops(),"l99997")
loop3.unroll(UNROLL_RATE=2)
fct.display ()
fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.loop_fusion()
fct.display ()

# RECIPE_SIMPLIFICATION++

fct.partial_eval()
fct.simplify_control()
fct.dead_code_elimination()
fct.display ()
# RECIPE PARALLELIZATION
fct.privatize_module()
fct.coarse_grain_parallelization()
fct.display ()


# RECIPE CLEAN_LABELS
fct.clean_labels()
fct.display ()
# RECIPE POSTAMBLE
ws.close()
