#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 1000
#endif

void Matrix_Mult(float a[N][N], float b[N][N], float c[N][N])
{
  int i, j, k;

 loop0:
  for(i = 0; i <N; i ++) 
  loop1:
    for(k = 0; k <N; k ++)
    loop2:
      if (1)
	for(j = 0; j <N; j ++)
	  c[i][j] = c[i][j]+a[i][k]*b[k][j];
  
}

int main()
{
  float (*a)[N][N],(*b)[N][N],(*c)[N][N];
  a=malloc((N)*(N)*sizeof(float));
  b=malloc((N)*(N)*sizeof(float));
  c=malloc((N)*(N)*sizeof(float));
  int i,j,k, n=N;
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      (*a)[i][j]=.5, (*b)[i][j]=.5,(*c)[i][j]=0;
  printf("calcul de Matrix_Mult\n");
  gettimeofday (&tvStart,NULL);
  Matrix_Mult(*a,*b,*c);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  return 0;
}
