#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 1000
#endif
#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))
#define  MOD(x,y) (x%y)

void Matrix_Mult(float a[N][N], float b[N][N], float c[N][N])
{
  int i, j, k;

 loop0:
  for(j = 0; j <N; j ++)
  loop1:
    for(k = 0; k <N; k ++)
    loop2:
      for(i = 0; i <N; i ++) {
 	c[i][j] = c[i][j]+a[i][k]*b[k][j];
      }
  
}



void Matrix_Mult_tile_32x32(float a[1000][1000], float b[1000][1000], float c[1000][1000])
{
   int i, j, k;
   //PIPS generated variable
   int i_t, k_t;

#pragma omp parallel for private(i_t,k_t,i,k,j)
   for(i_t = 0; i_t <= 31; i_t += 1)
      for(k_t = 0; k_t <= 31; k_t += 1)

         for(i = 32*i_t; i <= pips_min(999, 32*i_t+31); i += 1)
            for(k = 32*k_t; k <= pips_min(999, 32*k_t+31); k += 1)
               for(j = 0; j <= 999; j += 1)
                  c[i][j] = c[i][j]+a[i][k]*b[k][j];
}



int main()
{
  float (*a)[N][N],(*b)[N][N],(*c)[N][N];
  a=malloc((N)*(N)*sizeof(float));
  b=malloc((N)*(N)*sizeof(float));
  c=malloc((N)*(N)*sizeof(float));
  int i,j,k, n=N;
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      (*a)[i][j]=.5, (*b)[i][j]=.5,(*c)[i][j]=0;
  printf("calcul de Matrix_Mult_tile_32x32\n");
  gettimeofday (&tvStart,NULL);
  Matrix_Mult_tile_32x32(*a,*b,*c);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);

  return 0;
}
