#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#ifndef N
#define N 100
#endif

#define pips_min(a,b) ((a) < (b) ? (a) : (b))
#define pips_max(a,b) ((a) > (b) ? (a) : (b))

#define  EDIV(x,y) ((x<0)?((x-y+1)/y):((x)/y))

void Matrix_Mult(float a[N][N], float b[N][N], float c[N][N])
{
  int i, j, k;
   
  for(i = 0; i <N; i ++)
    for(j = 0; j <N; j ++) 
      c[i][j] = 0;

 loop0:
  for(j = 0; j <N; j ++)
  loop1:
    for(k = 0; k <N; k ++)
    loop2:
      for(i = 0; i <N; i ++) {
 	c[i][j] = c[i][j]+a[i][k]*b[k][j];
      }
  
}

void Matrix_Mult_omp(float a[100][100], float b[100][100], float c[100][100])
{
   int i, j;
   //PIPS generated variable
   int j_t, k_t, i_t;

   //#pragma omp parallel for private(j,i_t,k_t)
   for(i = 0; i <= 99; i += 1)
     // #pragma omp parallel for private(i_t,k_t)
      for(j = 0; j <= 99; j += 1)
         c[i][j] = 0;

   // #pragma omp parallel for private(i_t,k_t)
   for(j_t = 0; j_t <= 49; j_t += 1)
      for(k_t = 0; k_t <= 24; k_t += 1)
	#pragma omp parallel for 
         for(i_t = 0; i_t <= 99; i_t += 1) {
            c[i_t][2*j_t] = c[i_t][2*j_t]+a[i_t][4*k_t]*b[4*k_t][2*j_t]+a[i_t][4*k_t+1]*b[4*k_t+1][2*j_t]+a[i_t][4*k_t+2]*b[4*k_t+2][2*j_t]+a[i_t][4*k_t+3]*b[4*k_t+3][2*j_t];
            c[i_t][2*j_t+1] = c[i_t][2*j_t+1]+a[i_t][4*k_t]*b[4*k_t][2*j_t+1]+a[i_t][4*k_t+1]*b[4*k_t+1][2*j_t+1]+a[i_t][4*k_t+2]*b[4*k_t+2][2*j_t+1]+a[i_t][4*k_t+3]*b[4*k_t+3][2*j_t+1];
         }
}

int main()
{
  float (*a)[N][N],(*b)[N][N],(*c)[N][N];
  a=malloc((N)*(N)*sizeof(float));
  b=malloc((N)*(N)*sizeof(float));
  c=malloc((N)*(N)*sizeof(float));
  int i,j,k, n=N;
  struct timeval tvStart,tvEnd;
  double linStart = 0,linEnd = 0,lTime = 0;

  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
	(*a)[i][j]=.5, (*b)[i][j]=.5, (*c)[i][j]=.5;
  
  gettimeofday (&tvStart,NULL);
  Matrix_Mult_omp(*a,*b,*c);
  gettimeofday (&tvEnd,NULL);

  linStart = ((double)tvStart.tv_sec * 1000 + (double)(tvStart.tv_usec/1000.0));
  linEnd = ((double)tvEnd.tv_sec * 1000 + (double)(tvEnd.tv_usec/1000.0));
  lTime = linEnd-linStart;
  printf("*** Minimum global time  : %.3f (ms)\n",lTime);
  return 0;
}
